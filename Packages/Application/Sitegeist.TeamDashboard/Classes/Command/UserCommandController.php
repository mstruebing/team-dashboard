<?php

namespace Sitegeist\TeamDashboard\Command;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Cli\CommandController;
use Neos\Flow\Security\AccountFactory;
use Neos\Flow\Security\AccountRepository;
use Neos\Flow\Persistence\PersistenceManagerInterface;

/**
 * This CommandController is providing CLI-commands for flow
 * to create, update and delete admin users
 */
class UserCommandController extends CommandController
{
    /**
     * @var AccountRepository
     * @Flow\Inject
     */
    protected $accountRepository;

    /**
     * @var AccountFactory
     * @Flow\Inject
     */
    protected $accountFactory;

    /**
     * @var PersistenceManagerInterface
     * @Flow\Inject
     */
    protected $persistenceManager;


    /**
     * Lists all available admin users on stdout
     */
    public function listCommand()
    {
        $accounts = $this->accountRepository->findAll();
        foreach ($accounts as $account) {
            printf("%s\n", $account->getAccountIdentifier());
        }
    }

    /**
     * creates an admin user
     *
     * @param string $username
     * @param string $password
     * @return void
     */
    public function createCommand(string $username, string $password)
    {
        $identifier = trim($username);
        $password = $password;
        $roles = array('Sitegeist.TeamDashboard:Administrator');
        $authenticationProviderName = 'DefaultProvider';

        $account = $this->accountFactory->createAccountWithPassword(
            $identifier,
            $password,
            $roles,
            $authenticationProviderName
        );

        $this->accountRepository->add($account);
    }

    /**
     * Deletes an admin user
     *
     * @param string $username
     * @return void
     */
    public function deleteCommand(string $username)
    {
        $identifier = trim($username);
        $account = $this->accountRepository->findOneByAccountIdentifier($username);
        $this->accountRepository->remove($account);
    }
}
