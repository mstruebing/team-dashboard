<?php

namespace Sitegeist\TeamDashboard\Controller;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 *
 * This class handles everything regarding authentication.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\ActionRequest;
use Neos\Flow\Security\Authentication\Controller\AbstractAuthenticationController;
use Neos\Flow\Security\Exception\AuthenticationRequiredException;

class AuthenticationController extends AbstractAuthenticationController
{
    /**
     * This function is triggered when an authentication where successful
     *
     * @param ActionRequest $originalRequest
     * @return string
     */
    protected function onAuthenticationSuccess(ActionRequest $originalRequest = null) : string
    {
        $this->response->setStatus(204);
        return '';
    }

    /**
     * This function is triggered when an authentication where unsuccessful
     *
     * @param AuthenticationRequiredException $exception
     * @return string
     */
    protected function onAuthenticationFailure(AuthenticationRequiredException $exception = null) : string
    {
        $this->response->setStatus(401);
        return '';
    }

    /**
     * This function is triggering an logout and
     * cause the AbstactAuthenticationController to log( out all active session tokens
     *
     * @return string
     */
    public function logoutAction() : string
    {
        parent::logoutAction();
        $this->response->setStatus(204);
        return '';
    }
}
