<?php
namespace Sitegeist\TeamDashboard\Controller;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 *
 * This class handles all API operations regarding Customers
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\RestController;

use Sitegeist\TeamDashboard\Domain\Model\Customer;
use Sitegeist\TeamDashboard\Domain\Repository\CustomerRepository;
use Sitegeist\TeamDashboard\Domain\Repository\ProjectRepository;

class CustomerController extends RestController
{
    /**
     * @var string
     */
    protected $resourceArgumentName = 'customer';

    /**
     * @var string
     */
    protected $defaultViewObjectName = 'Neos\\Flow\\Mvc\\View\\JsonView';

    /**
     * @Flow\Inject
     * @var CustomerRepository
     */
    protected $customerRepository;

    /**
     * @Flow\Inject
     * @var ProjectRepository
     */
    protected $projectRepository;

    /**
     * Index action needed for elm to return a 2xx for OPTIONS request
     * before PUT, POST and DELETE requests
     *
     * @Flow\SkipCsrfProtection
     * @return void
     */
    public function indexAction()
    {
        $this->response->setStatus(204);
    }

    /**
     * Lists all customers
     *
     * @return void
     */
    public function listAction()
    {
        $this->view->setVariablesToRender(array('customers'));
        $customers = $this->customerRepository->findAll();
        $this->view->assign('customers', $customers);
    }

    /**
     * Creates a customer
     *
     * @param array $customer
     * @return void
     */
    public function createAction(array $customer)
    {
        if ($this->hasValidName($customer, 'name') && !$this->alreadyInRepository($customer['name'])) {
            $newCustomer = new Customer();
            $newCustomer->setName($customer['name']);
            $this->customerRepository->add($newCustomer);

            $this->view->setVariablesToRender(array('customer'));
            $this->view->assign('customer', $newCustomer);
            $this->response->setStatus(201);
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'No Customer name provided or already in repository');
            $this->response->setStatus(400);
        }
    }

    /**
     * Updates an existing customer
     *
     * @param array $customer
     * @return void
     */
    public function updateAction(array $customer)
    {
        if ($this->hasValidName($customer, 'oldCustomer')
            && $this->alreadyInRepository($customer['oldCustomer'])
            && $this->hasValidName($customer, 'newCustomer')) {
            $customerToChange = $this->customerRepository->findOneByName($customer['oldCustomer']);
            $customerToChange->setName($customer['newCustomer']);
            $this->customerRepository->update($customerToChange);

            $this->view->setVariablesToRender(array('customer'));
            $this->view->assign('customer', $customerToChange);
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'No valid oldCustomer or newCustomer name or oldCustomer not in repository');
            $this->response->setStatus(400);
        }
    }

    /**
     * Deletes an existing customer
     *
     * @param array $customer
     * @return void
     */
    public function deleteAction(array $customer)
    {
        if ($this->hasValidName($customer, 'name') && $this->alreadyInRepository($customer['name'])) {
            $customerToDelete = $this->customerRepository->findOneByName($customer['name']);


            /* set all customers to null on projects where the customer is the */
            /* customer which should be deleted */
            $projects = $this->projectRepository->findByCustomer($customerToDelete);
            foreach ($projects as $project) {
                $project->setCustomer(null);
                $this->projectRepository->update($project);
            }

            $this->customerRepository->remove($customerToDelete);

            $this->view->setVariablesToRender(array('customer'));
            $this->view->assign('customer', $customerToDelete);
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'No valid customer name or not in repository');
            $this->response->setStatus(400);
        }
    }

    /**
     * Checks wether a customer is already in the repository or not
     *
     * @param string $name
     * @return bool
     */
    protected function alreadyInRepository(string $name) : bool
    {
        return $this->customerRepository->findByName($name)->count() !== 0;
    }

    /**
     * Checks wether a customer array contains a valid name
     *
     * @param array $customer
     * @param string $index
     * @return bool
     */
    protected function hasValidName(array $customer, string $index) : bool
    {
        return isset($customer[$index]) && strlen($customer[$index]) > 0;
    }
}
