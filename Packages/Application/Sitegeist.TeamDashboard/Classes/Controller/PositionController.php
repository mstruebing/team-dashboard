<?php
namespace Sitegeist\TeamDashboard\Controller;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 *
 * This class handles all API operations regarding Positions
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\RestController;

use Sitegeist\TeamDashboard\Domain\Model\Position;
use Sitegeist\TeamDashboard\Domain\Repository\PositionRepository;
use Sitegeist\TeamDashboard\Domain\Repository\UserRepository;

class PositionController extends RestController
{
    /**
     * @var string
     */
    protected $resourceArgumentName = 'position';

    /**
     * @var string
     */
    protected $defaultViewObjectName = 'Neos\\Flow\\Mvc\\View\\JsonView';

    /**
     * @Flow\Inject
     * @var PositionRepository
     */
    protected $positionRepository;

    /**
     * @Flow\Inject
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * Index action needed for elm to return a 2xx for OPTIONS request
     * before PUT, POST and DELETE requests
     *
     * @Flow\SkipCsrfProtection
     * @return void
     */
    public function indexAction()
    {
    }

    /**
     * List all positions
     *
     * @return void
     */
    public function listAction()
    {
        $positions = $this->positionRepository->findAll();
        $this->view->setVariablesToRender(array('positions'));
        $this->view->assign('positions', $positions);
    }

    /**
     * Creates a position
     *
     * @param array $position
     * @return void
     */
    public function createAction(array $position)
    {
        if ($this->hasValidLabel($position, 'label') && !$this->alreadyInRepository(ucwords($position['label']))) {
            $newPosition = new position();
            $newPosition->setLabel(ucwords($position['label']));
            $this->positionRepository->add($newPosition);

            $this->view->setVariablesToRender(array('position'));
            $this->view->assign('position', $newPosition);
            $this->response->setStatus(201);
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'No Position name provided or already in repository');
            $this->response->setStatus(400);
        }
    }

    /**
     * Updates an existing position
     *
     * @param array $position
     * @return void
     */
    public function updateAction(array $position)
    {
        if ($this->hasValidLabel($position, 'oldPosition')
            && $this->hasValidLabel($position, 'newPosition')
            && $this->alreadyInRepository(ucwords($position['oldPosition']))) {
            $positionToUpdate = $this->positionRepository->findOneByLabel(ucwords($position['oldPosition']));
            $positionToUpdate->setLabel(ucwords($position['newPosition']));
            $this->positionRepository->update($positionToUpdate);

            $this->view->setVariablesToRender(array('position'));
            $this->view->assign('position', $positionToUpdate);
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'No oldPosition or newPosition  provided or not in repository');
            $this->response->setStatus(400);
        }
    }

    /**
     * Deletes an existing position
     *
     * @param array $position
     * @return void
     */
    public function deleteAction(array $position)
    {
        if ($this->hasValidLabel($position, 'label') && $this->alreadyInRepository(ucwords($position['label']))) {
            $positionToDelete = $this->positionRepository->findOneByLabel(ucwords($position['label']));

            /* removes the position which should be deleted from every user */
            $usersWithPosition = $this->userRepository->findByPosition($positionToDelete);
            foreach ($usersWithPosition as $user) {
                $user->removePosition();
                $this->userRepository->update($user);
            }

            $this->positionRepository->remove($positionToDelete);

            $this->view->setVariablesToRender(array('position'));
            $this->view->assign('position', $positionToDelete);
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'No Position name provided or not in repository');
            $this->response->setStatus(400);
        }
    }

    /**
     * Checks wether a request is valid or not
     *
     * @param array $position
     * @param string $index
     * @return bool
     */
    protected function hasValidLabel(array $position, string $index) : bool
    {
        return isset($position[$index]) && strlen($position[$index]) >= 2;
    }

    /**
     * Checks if a position is already in the repository
     *
     * @param string $label
     * @return bool
     */
    protected function alreadyInRepository(string $label) : bool
    {
        return $this->positionRepository->findByLabel($label)->count() !== 0;
    }
}
