<?php
namespace Sitegeist\TeamDashboard\Controller;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 *
 * This class handles all API operations regarding Projects
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\RestController;

use Sitegeist\TeamDashboard\Domain\Model\Project;
use Sitegeist\TeamDashboard\Domain\Repository\CustomerRepository;
use Sitegeist\TeamDashboard\Domain\Repository\ProjectRepository;
use Sitegeist\TeamDashboard\Domain\Repository\UserRepository;
use Sitegeist\TeamDashboard\Domain\Repository\TeamRepository;

class ProjectController extends RestController
{
    /**
     * @var string
     */
    protected $resourceArgumentName = 'project';

    /**
     * @var string
     */
    protected $defaultViewObjectName = 'Neos\\Flow\\Mvc\\View\\JsonView';

    /**
     * @Flow\Inject
     * @var ProjectRepository
     */
    protected $projectRepository;

    /**
     * @Flow\Inject
     * @var CustomerRepository
     */
    protected $customerRepository;

    /**
     * @Flow\Inject
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @Flow\Inject
     * @var TeamRepository
     */
    protected $teamRepository;

    /**
     * Index action needed for elm to return a 2xx for OPTIONS request
     * before PUT, POST and DELETE requests
     *
     * @Flow\SkipCsrfProtection
     * @return void
     */
    public function indexAction()
    {
    }

    /**
     * List all projects
     *
     * @return void
     */
    public function listAction()
    {
        $this->view->setVariablesToRender(array('projects'));
        $rawProjects = $this->projectRepository->findAll();
        $projects = array();

        /* project didn't only consist of simple data types */
        foreach ($rawProjects as $project) {
            array_push($projects, $this->generateResponseArray($project));
        }

        $this->view->assign('projects', $projects);
    }

    /**
     * Creates a project
     *
     * @param array $project
     * @return void
     */
    public function createAction(array $project)
    {
        if ($this->hasValidName($project, 'name')
            && $this->hasValidName($project, 'quoJobNr')
            && !$this->alreadyInRepository($project)) {
            $newProject = new project();
            $newProject->setName($project['name']);
            $newProject->setQuoJobNr($project['quoJobNr']);

            $newProject->setQuoJobClient($project['quoJobClient'] ?? '');
            $newProject->setGitlab($project['gitlab'] ?? '');
            $newProject->setRedmine($project['redmine'] ?? '');

            if (isset($project['customer'])) {
                $customer = $this->customerRepository->findOneByName($project['customer']);
                $newProject->setCustomer($customer);
            }

            if (isset($project['leadPm'])) {
                $leadPm = $this->userRepository->findOneByEmail($project['leadPm']);
                $newProject->setLeadPm($leadPm);
            }

            if (isset($project['leadDev'])) {
                $leadDev = $this->userRepository->findOneByEmail($project['leadDev']);
                $newProject->setLeadDev($leadDev);
            }

            if (isset($project['team'])) {
                $team = $this->teamRepository->findOneByName($project['team']);
                $newProject->setTeam($team);
            }

            $this->projectRepository->add($newProject);

            $this->view->setVariablesToRender(array('project'));
            $this->view->assign('project', $this->generateResponseArray($newProject));
            $this->response->setStatus(201);
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'No project name or quoJobNr provided or already in repository');
            $this->response->setStatus(400);
        }
    }

    /**
     * Updates an existing project
     *
     * @param array $project
     * @return void
     */
    public function updateAction(array $project)
    {
        if (isset($project['oldProject']['name']) &&
            isset($project['newProject']['name']) &&
            $this->projectRepository->findByName($project['oldProject']['name'])->count() !== 0) {
            $projectToUpdate = $this->projectRepository->findOneByNameAndQuojobNr($project['oldProject']);

            $projectToUpdate->setName($project['newProject']['name']);
            $projectToUpdate->setQuoJobNr($project['newProject']['quoJobNr']);

            $projectToUpdate->setGitlab($project['newProject']['gitlab'] ?? '');
            $projectToUpdate->setRedmine($project['newProject']['redmine'] ?? '');
            $projectToUpdate->setQuoJobClient($project['newProject']['quoJobClient'] ?? '');

            if (isset($project['newProject']['customer'])) {
                $customer = $this->customerRepository->findOneByName($project['newProject']['customer']);
                $projectToUpdate->setCustomer($customer);
            } else {
                $projectToUpdate->setCustomer($null);
            }

            if (isset($project['newProject']['leadPm'])) {
                $leadPm = $this->userRepository->findOneByEmail($project['newProject']['leadPm']);
                $projectToUpdate->setLeadPm($leadPm);
            } else {
                $projectToUpdate->setLeadPm(null);
            }

            if (isset($project['newProject']['leadDev'])) {
                $leadDev = $this->userRepository->findOneByEmail($project['newProject']['leadDev']);
                $projectToUpdate->setLeadDev($leadDev);
            } else {
                $projectToUpdate->setLeadDev(null);
            }

            if (isset($project['newProject']['team'])) {
                $team = $this->teamRepository->findOneByName($project['newProject']['team']);
                $projectToUpdate->setTeam($team);
            } else {
                $projectToUpdate->setTeam(null);
            }

            $this->projectRepository->update($projectToUpdate);

            $this->view->setVariablesToRender(array('project'));
            $this->view->assign('project', $this->generateResponseArray($projectToUpdate));
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'No project name or quoJobNr provided or not in repository');
            $this->response->setStatus(400);
        }
    }

    /**
     * Deletes an existing project
     *
     * @param array $project
     * @return void
     */
    public function deleteAction(array $project)
    {
        if ($this->hasValidName($project, 'name') && $this->alreadyInRepository($project)) {
            if (isset($project['customer'])) {
                $customer = $this->customerRepository->findOneByName($project['customer']);
            } else {
                $customer = null;
            }

            $projectToDelete = $this->projectRepository->findOneByNameAndQuojobNr($project);

            $this->projectRepository->remove($projectToDelete);

            $this->view->setVariablesToRender(array('project'));
            $this->view->assign('project', $this->generateResponseArray($projectToDelete));
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'No project name provided or not in repository');
            $this->response->setStatus(400);
        }
    }

    /**
     * Checks wether a project is already in the repository or not
     *
     * @param array $project
     * @return bool
     */
    protected function alreadyInRepository(array $project)
    {
        return $this->projectRepository->findOneByNameAndQuojobNr($project) !== null;
    }

    /**
     * Checks wether a project name is valid or not
     *
     * @param array $project
     * @param string $index
     * @return bool
     */
    protected function hasValidName(array $project, string $index) : bool
    {
        return isset($project[$index]) && strlen($project[$index]) > 0;
    }

    /**
     * Generates an array which is needed for the json response
     * because the object doesn't only contains simple data types
     *
     * @param Project $project
     * @return array
     */
    protected function generateResponseArray(Project $project) : array
    {
        $responseArray = array();

        $responseArray['name'] = $project->getName();
        $responseArray['quoJobNr'] = $project->getQuoJobNr();
        $responseArray['quoJobClient'] = $project->getQuoJobClient();
        $responseArray['gitlab'] = $project->getGitlab();
        $responseArray['redmine'] = $project->getRedmine();

        if ($project->getLeadPm() !== null) {
            $responseArray['leadPm'] = $project->getLeadPm()->getEmail();
        } else {
            $responseArray['leadPm'] = '';
        }

        if ($project->getLeadDev() !== null) {
            $responseArray['leadDev'] = $project->getLeadDev()->getEmail();
        } else {
            $responseArray['leadDev'] = '';
        }

        if ($project->getCustomer() !== null) {
            $responseArray['customer'] = $project->getCustomer()->getName();
        } else {
            $responseArray['customer'] = '';
        }

        if ($project->getTeam() !== null) {
            $responseArray['team'] = $project->getTeam()->getName();
        } else {
            $responseArray['team'] = '';
        }

        return $responseArray;
    }
}
