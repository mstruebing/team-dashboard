<?php
namespace Sitegeist\TeamDashboard\Controller;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\RestController;

use Sitegeist\TeamDashboard\Domain\Model\Skill;
use Sitegeist\TeamDashboard\Domain\Repository\SkillRepository;
use Sitegeist\TeamDashboard\Domain\Repository\UserRepository;

class SkillController extends RestController
{
    /**
     * @var string
     */
    protected $resourceArgumentName = 'skill';

    /**
     * @var string
     */
    protected $defaultViewObjectName = 'Neos\\Flow\\Mvc\\View\\JsonView';

    /**
     * @var SkillRepository
     * @Flow\Inject
     */
    protected $skillRepository;

    /**
     * @var UserRepository
     * @Flow\Inject
     */
    protected $userRepository;

    /**
     * Index action needed for elm to return a 2xx for OPTIONS request
     * before PUT, POST and DELETE requests
     *
     * @Flow\SkipCsrfProtection
     * @return void
     */
    public function indexAction()
    {
    }

    /**
     * List all skills
     *
     * @return void
     */
    public function listAction()
    {
        $skills = $this->skillRepository->findAll();
        $this->view->setVariablesToRender(array('skills'));
        $this->view->assign('skills', $skills);
    }

    /**
     * Creates a skill
     *
     * @param array $skill
     * @return void
     */
    public function createAction(array $skill)
    {
        if ($this->hasValidLabel($skill, 'label')
            && !$this->alreadyInRepository($skill['label'])) {
            $normalizedLabel = $this->normalizeLabel($skill['label']);

            $newSkill = new Skill();
            $newSkill->setLabel($normalizedLabel);
            $this->skillRepository->add($newSkill);

            $this->view->setVariablesToRender(array('skill'));
            $this->view->assign('skill', $newSkill);
            $this->response->setStatus(201);
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'Not a valid label or already in repository');
            $this->response->setStatus(400);
        }
    }


    /**
     * Updates an existing skill
     *
     * @param array $skill
     * @return void
     */
    public function updateAction(array $skill)
    {
        if ($this->hasValidLabel($skill, 'oldSkill')
            && $this->hasValidLabel($skill, 'newSkill')
            && $this->alreadyInRepository($skill['oldSkill'])
            && !$this->alreadyInRepository($skill['newSkill'])) {
            $skillToUpdate = $this->skillRepository->findOneByLabel($this->normalizeLabel($skill['oldSkill']));
            $skillToUpdate->setLabel($this->normalizeLabel($skill['newSkill']));
            $this->skillRepository->update($skillToUpdate);

            $this->view->setVariablesToRender(array('skill'));
            $this->view->assign('skill', $skillToUpdate);
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign(
                'error',
                'Not a valid of newSkill or oldSkill or oldSkill not in repository or newSkill already in repository'
            );
            $this->response->setStatus(400);
        }
    }

    /**
     * Deletes an existing skill
     *
     * @param array $skill
     * @return void
     */
    public function deleteAction(array $skill)
    {
        if ($this->hasValidLabel($skill, 'label') &&
            $this->alreadyInRepository($skill['label'])) {
            $normalizedLabel = $this->normalizeLabel($skill['label']);
            $skillToDelete = $this->skillRepository->findOneByLabel($normalizedLabel);

            // Remove the skill from all users
            $users = $this->userRepository->findAll();
            foreach ($users as $user) {
                foreach ($user->getSkills() as $userSkill) {
                    if ($userSkill === $skillToDelete) {
                        $user->removeSkill($skillToDelete);
                        $this->userRepository->update($user);
                    }
                }
            }

            $this->skillRepository->remove($skillToDelete);
            $this->view->setVariablesToRender(array('skill'));
            $this->view->assign('skill', $skillToDelete);
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'Not a valid label or already in repository');
            $this->response->setStatus(400);
        }
    }

    /**
     * Checks wether a label is valid or not
     *
     * @param array $skill
     * @param string $index
     * @return bool
     */
    protected function hasValidLabel(array $skill, string $index) : bool
    {
        return isset($skill[$index]) && strlen($this->normalizeLabel($skill[$index])) >= 3;
    }

    /**
     * Normalize a label, removes all non alphanumeric characers
     *
     * @param string $label
     * @return string
     */
    protected function normalizeLabel(string $label) : string
    {
        return strtolower(preg_replace("/[^A-Za-z0-9]/", '', $label));
    }

    /**
     * Checks if a skill is already in the repository
     *
     * @param string $label
     * @return bool
     */
    protected function alreadyInRepository(string $label) : bool
    {
        return $this->skillRepository->findByLabel($this->normalizeLabel($label))->count() !== 0;
    }
}
