<?php
namespace Sitegeist\TeamDashboard\Controller;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\RestController;

use Sitegeist\TeamDashboard\Domain\Model\Slogan;
use Sitegeist\TeamDashboard\Domain\Repository\SloganRepository;

class SloganController extends RestController
{
    /**
     * @var string
     */
    protected $resourceArgumentName = 'slogan';

    /**
     * @var string
     */
    protected $defaultViewObjectName = 'Neos\\Flow\\Mvc\\View\\JsonView';


    /**
     * @Flow\Inject
     * @var SloganRepository
     */
    protected $sloganRepository;

    /**
     * Index action needed for elm to return a 2xx for OPTIONS request
     * before PUT, POST and DELETE requests
     *
     * @Flow\SkipCsrfProtection
     * @return void
     */
    public function indexAction()
    {
    }

    /**
     * List all slogans
     *
     * @return void
     */
    public function listAction()
    {
        $slogans = $this->sloganRepository->findAll();

        /* slogans is an object but shuffle needs an array */
        $randomizedSlogans = array();
        foreach ($slogans as $slogan) {
            array_push($randomizedSlogans, $slogan);
        }

        /* randomize because elm can't do random at the beginning */
        /* this would result in showing the same slogan on first every time the index */
        /* is called first time */
        shuffle($randomizedSlogans);

        $this->view->setVariablesToRender(array('slogans'));
        $this->view->assign('slogans', $randomizedSlogans);
    }

    /**
     * Creates a slogan
     *
     * @param array $slogan
     * @return void
     */
    public function createAction(array $slogan)
    {
        $newSlogan = new Slogan($slogan);
        $this->sloganRepository->add($newSlogan);

        $this->view->setVariablesToRender(array('slogan'));
        $this->view->assign('slogan', $newSlogan);
        $this->response->setStatus(201);
    }

    /**
     * Updates an existing slogan
     *
     * @param array $slogan
     * @return void
     */
    public function updateAction(array $slogan)
    {
        $sloganToUpdate = $this->sloganRepository->findOneByText($slogan['oldSlogan']);
        $sloganToUpdate->setText($slogan['newSlogan']);
        $this->sloganRepository->update($sloganToUpdate);

        $this->view->setVariablesToRender(array('slogan'));
        $this->view->assign('slogan', $sloganToUpdate);
    }

    /**
     * @param array $slogan
     * @return void
     */
    public function deleteAction(array $slogan)
    {
        $sloganToDelete = $this->sloganRepository->findOneByText($slogan['slogan']);
        $this->sloganRepository->remove($sloganToDelete);
        $this->view->setVariablesToRender(array('slogan'));
        $this->view->assign('slogan', $sloganToDelete);
    }
}
