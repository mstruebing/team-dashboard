<?php
namespace Sitegeist\TeamDashboard\Controller;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\RestController;

use Sitegeist\TeamDashboard\Domain\Model\Team;
use Sitegeist\TeamDashboard\Domain\Repository\ProjectRepository;
use Sitegeist\TeamDashboard\Domain\Repository\SkillRepository;
use Sitegeist\TeamDashboard\Domain\Repository\TeamRepository;
use Sitegeist\TeamDashboard\Domain\Repository\UserRepository;

class TeamController extends RestController
{
    /**
     * @var string
     */
    protected $resourceArgumentName = 'team';

    /**
     * @var string
     */
    protected $defaultViewObjectName = 'Neos\\Flow\\Mvc\\View\\JsonView';

    /**
     * @Flow\Inject
     * @var TeamRepository
     */
    protected $teamRepository;

    /**
     * @Flow\Inject
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @Flow\Inject
     * @var ProjectRepository
     */
    protected $projectRepository;

    /**
     * @Flow\Inject
     * @var SkillRepository
     */
    protected $skillRepository;

    /**
     * Index action needed for elm to return a 2xx for OPTIONS request
     * before PUT, POST and DELETE requests
     *
     * @Flow\SkipCsrfProtection
     * @return void
     */
    public function indexAction()
    {
    }

    /**
     * List all teams
     *
     * @return void
     */
    public function listAction()
    {
        $teams = $this->teamRepository->findAll();
        $teamsWithCoachAndPm = array();

        /* team didn't only consist of simple data types */
        foreach ($teams as $team) {
            array_push($teamsWithCoachAndPm, $this->generateResponseArray($team));
        }

        $this->view->setVariablesToRender(array('teamsWithCoachAndPm'));
        $this->view->assign('teamsWithCoachAndPm', $teamsWithCoachAndPm);
    }

    /**
     * Creates a team
     *
     * @param array $team
     * @return void
     */
    public function createAction(array $team)
    {
        if ($this->hasValidName($team, 'name') && !$this->alreadyInRepository($team['name'])) {
            $newTeam = new team();
            $newTeam->setName($team['name']);
            $newTeam->setKanban($team['kanban']);
            $newTeam->setTeamBm($this->userRepository->findOneByEmail($team['teamBm']));
            $newTeam->setTeamCoach($this->userRepository->findOneByEmail($team['teamCoach']));

            $this->teamRepository->add($newTeam);

            $this->view->setVariablesToRender(array('team'));
            $this->view->assign('team', $this->generateResponseArray($newTeam));
            $this->response->setStatus(201);
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'No team name provided');
            $this->response->setStatus(400);
        }
    }

    /**
     * Updates a team
     *
     * @param array $team
     * @return void
     */
    public function updateAction(array $team)
    {
        if (isset($team['oldTeam']['name'])
            && $this->alreadyInRepository($team['oldTeam']['name'])
            && isset($team['newTeam']['name'])
            && (!$this->alreadyInRepository($team['newTeam']['name'])
            || $team['newTeam']['name'] === $team['oldTeam']['name'])) {
            $teamToChange = $this->teamRepository->findOneByName($team['oldTeam']['name']);
            $teamToChange->setName($team['newTeam']['name']);

            if (isset($team['newTeam']['kanban'])) {
                $teamToChange->setKanban($team['newTeam']['kanban']);
            }
            if (isset($team['newTeam']['teamBm'])) {
                $teamToChange->setTeamBm($this->userRepository->findOneByEmail($team['newTeam']['teamBm']));
            }

            if (isset($team['newTeam']['teamCoach'])) {
                $teamToChange->setTeamCoach($this->userRepository->findOneByEmail($team['newTeam']['teamCoach']));
            }

            $this->teamRepository->update($teamToChange);

            $this->view->setVariablesToRender(array('team'));
            $this->view->assign('team', $this->generateResponseArray($teamToChange));
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign(
                'error',
                'Not a valid name of newTeam or oldTeam or oldTeam not in repository or newTeam already in repository'
            );
            $this->response->setStatus(400);
        }
    }

    /**
     * Deletes a team
     *
     * @param array $team
     * @return void
     */
    public function deleteAction(array $team)
    {
        if ($this->hasValidName($team, 'name') && $this->alreadyInRepository($team['name'])) {
            $teamToDelete = $this->teamRepository->findOneByName($team['name']);

            /* remove the team from every assigned user */
            $users = $this->userRepository->findByTeam($teamToDelete);
            foreach ($users as $user) {
                $user->setTeam(null);
                $this->userRepository->update($user);
            }

            /* remove the team from every assigned project */
            $projects = $this->projectRepository->findByTeam($teamToDelete);
            foreach ($projects as $project) {
                $project->setTeam(null);
                $this->projectRepository->update($team);
            }

            $this->teamRepository->remove($teamToDelete);

            $this->view->setVariablesToRender(array('team'));
            $this->view->assign('team', $this->generateResponseArray($teamToDelete));
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'No team name provided or not in repository');
            $this->response->setStatus(400);
        }
    }

    /**
     * Checks if a team is already in the repository
     *
     * @param string $label
     * @return bool
     */
    protected function alreadyInRepository(string $name) : bool
    {
        return $this->teamRepository->findByName($name)->count() !== 0;
    }

    /**
     * Checks wether a team array contains a valid name
     *
     * @param array $team
     * @param string $index
     * @return bool
     */
    protected function hasValidName(array $team, string $index) : bool
    {
        return isset($team[$index]) && strlen($team[$index]) > 0;
    }

    /**
     * Generates an array which is needed for the json response
     * because the object doesn't only contains simple data types
     *
     * @param Team $team
     * @return array
     */
    protected function generateResponseArray(Team $team) : array
    {
        $responseArray = array();
        $responseArray['name'] = $team->getName();
        $responseArray['kanban'] = $team->getKanban();
        if ($team->getTeamBm() !== null) {
            $responseArray['teamBm'] = $team->getTeamBm()->getEmail();
        } else {
            $responseArray['teamBm'] = '';
        }

        if ($team->getTeamCoach() !== null) {
            $responseArray['teamCoach'] = $team->getTeamCoach()->getEmail();
        } else {
            $responseArray['teamCoach'] = '';
        }

        return $responseArray;
    }
}
