<?php
namespace Sitegeist\TeamDashboard\Controller;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\RestController;
use Neos\Flow\Validation\Validator\EmailAddressValidator;

use Sitegeist\TeamDashboard\Domain\Model\Position;
use Sitegeist\TeamDashboard\Domain\Model\Skill;
use Sitegeist\TeamDashboard\Domain\Model\User;
use Sitegeist\TeamDashboard\Domain\Repository\PositionRepository;
use Sitegeist\TeamDashboard\Domain\Repository\ProjectRepository;
use Sitegeist\TeamDashboard\Domain\Repository\SkillRepository;
use Sitegeist\TeamDashboard\Domain\Repository\TeamRepository;
use Sitegeist\TeamDashboard\Domain\Repository\UserRepository;

class UserController extends RestController
{
    /**
     * @var string
     */
    protected $resourceArgumentName = 'user';

    /**
     * @var string
     */
    protected $defaultViewObjectName = 'Neos\\Flow\\Mvc\\View\\JsonView';

    /**
     * @Flow\Inject
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @Flow\Inject
     * @var SkillRepository
     */
    protected $skillRepository;

    /**
     * @Flow\Inject
     * @var PositionRepository
     */
    protected $positionRepository;

    /**
     * @Flow\Inject
     * @var TeamRepository
     */
    protected $teamRepository;

    /**
     * @Flow\Inject
     * @var ProjectRepository
     */
    protected $projectRepository;

    /**
     * Index action needed for elm to return a 2xx for OPTIONS request
     * before PUT, POST and DELETE requests
     *
     * @Flow\SkipCsrfProtection
     * @return void
     */
    public function indexAction()
    {
    }

    /**
     * List all users
     *
     * @return void
     */
    public function listAction()
    {
        $users = $this->userRepository->findAll();
        $usersWithSkills = array();

        /* user didn't only consist of simple data types */
        foreach ($users as $user) {
            array_push($usersWithSkills, $this->generateResponseArray($user));
        }

        $this->view->setVariablesToRender(array('usersWithSkills'));
        $this->view->assign('usersWithSkills', $usersWithSkills);
    }

    /**
     * Creates a user
     *
     * @param array $user
     * @return void
     */
    public function createAction(array $user)
    {
        $newUser = new User();

        if (isset($user['name'])) {
            $newUser->setName($user['name']);
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'The user needs a name');
            $this->response->setStatus(400);

            return '';
        }

        if (isset($user['email'])) {
            $validator = new \Neos\Flow\Validation\Validator\EmailAddressValidator;
            $result = $validator->validate($user['email']);
            if (!$result->hasErrors()) {
                $newUser->setEmail($user['email']);
            } else {
                $this->view->setVariablesToRender(array('error'));
                $this->view->assign('error', 'The users email is not a valid email');
                $this->response->setStatus(400);

                return '';
            }
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'The users email is not a valid email');
            $this->response->setStatus(400);


            return '';
        }

        if (!$this->alreadyInRepository($user['email'])) {
            if (isset($user['shorthand']) && strlen($user['shorthand']) === 3) {
                if ($this->userRepository->findByShorthand($user['shorthand']) !== 0) {
                    $newUser->setShorthand($user['shorthand']);
                } else {
                    $this->view->setVariablesToRender(array('error'));
                    $this->view->assign('error', 'A user with this shorthand already exists');
                    $this->response->setStatus(400);

                    return '';
                }
            } else {
                $this->view->setVariablesToRender(array('error'));
                $this->view->assign('error', 'The shorthand is not valid');
                $this->response->setStatus(400);

                return '';
            }

            $newUser->setTelephone($user['telephone'] ?? '');
            $newUser->setTeam($this->teamRepository->findOneByName($user['team']));
            $newUser->setGitlab($user['gitlab'] ?? '');
            $newUser->setRedmine($user['redmine'] ?? '');
            $newUser->setWorkingHours($user['workingHours'] ?? '');
            $newUser->setRoom($user['room'] ?? '');

            if (isset($user['position']) && strlen($user['position']) > 0) {
                $newUser->setPosition($this->positionRepository->findOneByLabel($user['position']));
            }

            if (isset($user['skills']) && strlen($user['skills']) > 3) {
                foreach (explode(', ', $user['skills']) as $skillLabel) {
                    $skill = $this->skillRepository->findOneByLabel($skillLabel);
                    $newUser->addSkill($skill);
                }
            }

            $this->view->setVariablesToRender(array('user'));
            $this->view->assign('user', $this->generateResponseArray($newUser));
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'A user with this email already exists');
            $this->response->setStatus(400);

            return '';
        }

        $this->userRepository->add($newUser);
        $this->response->setStatus(201);
    }

    /**
     * Updates an existing user
     *
     * @param array $user
     * @return void
     */
    public function updateAction(array $user)
    {
        if (isset($user['oldUser']['email'])) {
            if ($this->alreadyInRepository($user['oldUser']['email']) && isset($user['newUser']['email'])) {
                $userToChange = $this->userRepository->findOneByEmail($user['oldUser']['email']);
                $userToChange->setEmail($user['newUser']['email'] ?? '');
                $userToChange->setName($user['newUser']['name'] ?? '');
                $userToChange->setTelephone($user['newUser']['telephone'] ?? '');
                $userToChange->setTeam($this->teamRepository->findOneByName($user['newUser']['team']));
                $userToChange->setGitlab($user['newUser']['gitlab'] ?? '');
                $userToChange->setShorthand($user['newUser']['shorthand'] ?? '');
                $userToChange->setRedmine($user['newUser']['redmine'] ?? '');
                $position = $this->positionRepository->findOneByLabel($user['newUser']['position']);
                if ($position !== null) {
                    $userToChange->setPosition($position);
                }
                $userToChange->setWorkingHours($user['newUser']['workingHours'] ?? '');
                $userToChange->setRoom($user['newUser']['room'] ?? '');
                foreach ($userToChange->getSkills() as $skill) {
                    $userToChange->removeSkill($skill);
                }
                if (isset($user['newUser']['skills']) && strlen($user['newUser']['skills']) >= 3) {
                    foreach (explode(', ', $user['newUser']['skills']) as $skillLabel) {
                        $skill = $this->skillRepository->findOneByLabel($skillLabel);
                        $userToChange->addSkill($skill);
                    }
                }

                $userToChange->setTeam($this->teamRepository->findOneByName($user['newUser']['team']));
                $this->userRepository->update($userToChange);

                $this->view->setVariablesToRender(array('user'));
                $this->view->assign('user', $this->generateResponseArray($userToChange));
            } else {
                $this->view->setVariablesToRender(array('error'));
                $this->view->assign('error', 'User not found');
                $this->response->setStatus(400);

                return '';
            }
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'No email provided');
            $this->response->setStatus(400);

            return '';
        }
    }

    /**
     * Deletes an existing user
     *
     * @param array $user
     * @return void
     */
    public function deleteAction(array $user)
    {
        if (isset($user['email']) && $this->alreadyInRepository($user['email'])) {
            $userToDelete = $this->userRepository->findOneByEmail($user['email']);

            /* removes the user as team bm from every team */
            $teams = $this->teamRepository->findByTeamBm($userToDelete);
            foreach ($teams as $team) {
                $team->setTeamBm(null);
                $this->teamRepository->update($team);
            }

            /* removes the user as team coach from every team */
            $teams = $this->teamRepository->findByTeamCoach($userToDelete);
            foreach ($teams as $team) {
                $team->setTeamCoach(null);
                $this->teamRepository->update($team);
            }

            /* removes the user as lead pm from every project */
            $projects = $this->projectRepository->findByLeadPm($userToDelete);
            foreach ($projects as $project) {
                $project->setLeadPm(null);
                $this->projectRepository->update($project);
            }

            /* removes the user as lead coach from every project */
            $projects = $this->projectRepository->findByLeadDev($userToDelete);
            foreach ($projects as $project) {
                $project->setLeadDev(null);
                $this->projectRepository->update($project);
            }

            $this->userRepository->remove($userToDelete);

            $this->view->setVariablesToRender(array('user'));
            $this->view->assign('user', $this->generateResponseArray($userToDelete));
        } else {
            $this->view->setVariablesToRender(array('error'));
            $this->view->assign('error', 'User not found');
            $this->response->setStatus(400);

            return '';
        }
    }

    /**
     * Checks if a user is already in the repository
     *
     * @param string $email
     * @return bool
     */
    protected function alreadyInRepository(string $email) : bool
    {
        return $this->userRepository->findByEmail($email)->count() !== 0;
    }

    /**
     * Generates an array which is needed for the json response
     * because the object doesn't only contains simple data types
     *
     * @param User $user
     * @return array
     */
    protected function generateResponseArray(User $user) : array
    {
        $responseArray = array();
        $responseArray['name'] = $user->getName();
        $responseArray['email'] = $user->getEmail() ?? '';
        $responseArray['shorthand'] = $user->getShorthand() ?? '';
        $responseArray['telephone'] = $user->getTelephone() ?? '';
        if ($user->getTeam() !== null) {
            $responseArray['team'] = $user->getTeam()->getName();
        } else {
            $responseArray['team'] = '';
        }
        $responseArray['gitlab'] = $user->getGitlab() ?? '';
        $responseArray['redmine'] = $user->getRedmine() ?? '';
        $responseArray['position'] = $user->getPosition() ?? new Position("");
        $responseArray['workingHours'] = $user->getWorkingHours() ?? '';
        $responseArray['room'] = $user->getRoom() ?? '';
        $responseArray['skills'] = array();
        $skills = $user->getSkills();
        foreach ($skills as $skill) {
            array_push($responseArray['skills'], $skill);
        }

        return $responseArray;
    }
}
