<?php
namespace Sitegeist\TeamDashboard\Domain\Model;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Project
{
    /**
     * @Flow\Validate(type="NotEmpty")
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $quoJobNr;

    /**
     * @var string
     */
    protected $quoJobClient;

    /**
     * @var string
     */
    protected $gitlab;

    /**
     * @var string
     */
    protected $redmine;

    /**
     * @ORM\ManyToOne
     * @var \Sitegeist\TeamDashboard\Domain\Model\Customer
     */
    protected $customer;

    /**
     * @ORM\ManyToOne
     * @var \Sitegeist\TeamDashboard\Domain\Model\User
     */
    protected $leadPm;

    /**
     * @ORM\ManyToOne
     * @var \Sitegeist\TeamDashboard\Domain\Model\User
     */
    protected $leadDev;

    /**
     * @ORM\ManyToOne
     * @var \Sitegeist\TeamDashboard\Domain\Model\Team
     */
    protected $team;


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getQuoJobNr()
    {
        return $this->quoJobNr;
    }

    /**
     * @param string $quoJobNr
     * @return void
     */
    public function setQuoJobNr($quoJobNr)
    {
        $this->quoJobNr = $quoJobNr;
    }

    /**
     * @return string
     */
    public function getQuoJobClient()
    {
        return $this->quoJobClient;
    }

    /**
     * @param string $quoJobClient
     * @return void
     */
    public function setQuoJobClient($quoJobClient)
    {
        $this->quoJobClient = $quoJobClient;
    }

    /**
     * @return string
     */
    public function getGitlab()
    {
        return $this->gitlab;
    }

    /**
     * @param string $gitlab
     * @return void
     */
    public function setGitlab($gitlab)
    {
        $this->gitlab = $gitlab;
    }

    /**
     * @return string
     */
    public function getRedmine()
    {
        return $this->redmine;
    }

    /**
     * @param string $redmine
     * @return void
     */
    public function setRedmine($redmine)
    {
        $this->redmine = $redmine;
    }

    /**
     * @return \Sitegeist\TeamDashboard\Domain\Model\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param \Sitegeist\TeamDashboard\Domain\Model\Customer $customer
     * @return void
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return \Sitegeist\TeamDashboard\Domain\Model\User
     */
    public function getLeadPm()
    {
        return $this->leadPm;
    }

    /**
     * @param \Sitegeist\TeamDashboard\Domain\Model\User $leadPm
     * @return void
     */
    public function setLeadPm($leadPm)
    {
        $this->leadPm = $leadPm;
    }

    /**
     * @return \Sitegeist\TeamDashboard\Domain\Model\User
     */
    public function getLeadDev()
    {
        return $this->leadDev;
    }

    /**
     * @param \Sitegeist\TeamDashboard\Domain\Model\User $leadDev
     * @return void
     */
    public function setLeadDev($leadDev)
    {
        $this->leadDev = $leadDev;
    }

    /**
     * @return \Sitegeist\TeamDashboard\Domain\Model\Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param \Sitegeist\TeamDashboard\Domain\Model\Team $team
     * @return void
     */
    public function setTeam($team)
    {
        $this->team = $team;
    }
}
