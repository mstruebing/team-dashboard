<?php
namespace Sitegeist\TeamDashboard\Domain\Model;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Slogan
{

    /**
     * @var string
     */
    protected $text;


    /**
     * @param mixed array $slogan
     */
    public function __construct(array $slogan)
    {
        $this->text = $slogan['slogan'];
    }


    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }
}
