<?php
namespace Sitegeist\TeamDashboard\Domain\Model;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Team
{
    /**
     * @Flow\Validate(type="NotEmpty")
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $kanban;

    /**
     * @ORM\ManyToOne
     * @var \Sitegeist\TeamDashboard\Domain\Model\User
     */
    protected $teamBm;

    /**
     * @ORM\ManyToOne
     * @var \Sitegeist\TeamDashboard\Domain\Model\User
     */
    protected $teamCoach;


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getKanban()
    {
        return $this->kanban;
    }

    /**
     * @param string $kanban
     * @return void
     */
    public function setKanban($kanban)
    {
        $this->kanban = $kanban;
    }

    /**
     * @return \Sitegeist\TeamDashboard\Domain\Model\User
     */
    public function getTeamBm()
    {
        return $this->teamBm;
    }

    /**
     * @param \Sitegeist\TeamDashboard\Domain\Model\User $teamBm
     * @return void
     */
    public function setTeamBm($teamBm)
    {
        $this->teamBm = $teamBm;
    }

    /**
     * @return \Sitegeist\TeamDashboard\Domain\Model\User
     */
    public function getTeamCoach()
    {
        return $this->teamCoach;
    }

    /**
     * @param \Sitegeist\TeamDashboard\Domain\Model\User $teamCoach
     * @return void
     */
    public function setTeamCoach($teamCoach)
    {
        $this->teamCoach = $teamCoach;
    }
}
