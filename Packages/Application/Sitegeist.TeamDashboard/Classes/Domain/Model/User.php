<?php
namespace Sitegeist\TeamDashboard\Domain\Model;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @Flow\Entity
 */
class User
{
    /**
     * @Flow\Validate(type="NotEmpty")
     * @var string
     */
    protected $name;

    /**
     * @Flow\Validate(type="string")
     * @var string
     */
    protected $email;

    /**
     * @Flow\Validate(type="StringLength", options={ "minimum"=3, "maximum"=3 })
     * @ORM\Column(length=3)
     * @var string
     */
    protected $shorthand;

    /**
     * @var string
     */
    protected $telephone;

    /**
     * @var string
     */
    protected $gitlab;

    /**
     * @var string
     */
    protected $redmine;

    /**
     * @ORM\ManyToOne
     * @var \Sitegeist\TeamDashboard\Domain\Model\Position
     */
    protected $position;

    /**
     * @ORM\ManyToOne
     * @var \Sitegeist\TeamDashboard\Domain\Model\Team
     */
    protected $team;

    /**
     * @ORM\ManyToMany
     * @var Collection<Skill>
     */
    protected $skills;

    /**
     * @var string
     */
    protected $room;

    /**
     * @var string
     */
    protected $workingHours;


    public function __construct()
    {
        $this->skills = new ArrayCollection();
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getShorthand()
    {
        return $this->shorthand;
    }

    /**
     * @param string $shorthand
     * @return void
     */
    public function setShorthand($shorthand)
    {
        $this->shorthand = $shorthand;
    }

    /**
     * @return integer
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param integer $telephone
     * @return void
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return \Sitegeist\TeamDashboard\Domain\Model\Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param \Sitegeist\TeamDashboard\Domain\Model\Team $team
     * @return void
     */
    public function setTeam($team)
    {
        $this->team = $team;
    }

    /**
     * @return string
     */
    public function getGitlab()
    {
        return $this->gitlab;
    }

    /**
     * @param string $gitlab
     * @return void
     */
    public function setGitlab($gitlab)
    {
        $this->gitlab = $gitlab;
    }

    /**
     * @return string
     */
    public function getRedmine()
    {
        return $this->redmine;
    }

    /**
     * @param string $redmine
     * @return void
     */
    public function setRedmine($redmine)
    {
        $this->redmine = $redmine;
    }

    /**
     * @return \Sitegeist\TeamDashboard\Domain\Model\Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param \Sitegeist\TeamDashboard\Domain\Model\Position $position
     * @return void
     */
    public function setPosition(\Sitegeist\TeamDashboard\Domain\Model\Position $position)
    {
        $this->position = $position;
    }

    /**
     * @return void
     */
    public function removePosition()
    {
        $this->position = null;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection<Skill>
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param \Sitegeist\TeamDashboard\Domain\Model\Skill $skill
     * @return void
     */
    public function addSkill(\Sitegeist\TeamDashboard\Domain\Model\Skill $skill)
    {
        $this->skills->add($skill);
    }

    /**
     * @param \Sitegeist\TeamDashboard\Domain\Model\Skill $skill
     * @return void
     */
    public function removeSkill(\Sitegeist\TeamDashboard\Domain\Model\Skill $skill)
    {
        $this->skills->removeElement($skill);
    }

    /**
     * @return string
     */
    public function getWorkingHours()
    {
        return $this->workingHours;
    }

    /**
     * @param string $workingHours
     * @return void
     */
    public function setWorkingHours(string $workingHours)
    {
        $this->workingHours = $workingHours;
    }

    /**
     * @param string $room
     * @return void
     */
    public function setRoom(string $room)
    {
        $this->room = $room;
    }

    /**
     * @return string
     */
    public function getRoom()
    {
        return $this->room;
    }
}
