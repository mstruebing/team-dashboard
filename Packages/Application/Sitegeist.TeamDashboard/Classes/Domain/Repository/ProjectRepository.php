<?php
namespace Sitegeist\TeamDashboard\Domain\Repository;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;
use Neos\Flow\Persistence\QueryInterface;
use Neos\Flow\Persistence\QueryResultInterface;

use Sitegeist\TeamDashboard\Domain\Model\Customer;
use Sitegeist\TeamDashboard\Domain\Model\Project;

/**
 * @Flow\Scope("singleton")
 */
class ProjectRepository extends Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = ['name' => \Neos\Flow\Persistence\QueryInterface::ORDER_ASCENDING];

    /**
     * Finds a project  by its customer and project name
     *
     * @param array $project
     * @param Customer $customer
     * @return Project
     */
    public function findOneByNameAndQuojobNr(array $project)
    {
        $query = $this->createQuery();
        return
            $query->matching(
                $query->logicalAnd([
                    $query->equals('quoJobNr', $project['quoJobNr']),
                    $query->equals('name', $project['name'])
                ])
            )
            ->execute()
            ->getFirst();
    }
}
