<?php
namespace Sitegeist\TeamDashboard\Domain\Repository;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class SkillRepository extends Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = ['label' => \Neos\Flow\Persistence\QueryInterface::ORDER_ASCENDING];
}
