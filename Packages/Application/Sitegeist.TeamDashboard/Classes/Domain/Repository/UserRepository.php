<?php
namespace Sitegeist\TeamDashboard\Domain\Repository;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;
use Neos\Flow\Persistence\QueryInterface;
use Neos\Flow\Persistence\QueryResultInterface;

use Sitegeist\TeamDashboard\Domain\Model\Skill;

/**
 * @Flow\Scope("singleton")
 */
class UserRepository extends Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = ['name' => \Neos\Flow\Persistence\QueryInterface::ORDER_ASCENDING];
}
