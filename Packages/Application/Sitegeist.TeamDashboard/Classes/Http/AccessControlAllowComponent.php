<?php
namespace Sitegeist\TeamDashboard\Http;

/*
 * This file is part of the Sitegeist.TeamDashboard package.
 */

use Neos\Flow\Http\Component\ComponentInterface;
use Neos\Flow\Http\Component\ComponentContext;

class AccessControlAllowComponent implements ComponentInterface
{

    /**
     * @var array
     */
    protected $options;

    /**
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        $this->options = $options;
    }

    /**
     * @param ComponentContext $componentContext
     * @return void
     */
    public function handle(ComponentContext $componentContext)
    {
        $httpResponse = $componentContext->getHttpResponse();

        /* Set Access-Control-Allow-Origin if present in the Configuration */
        if (isset($this->options['value'])) {
            $httpResponse->setHeader('Access-Control-Allow-Origin', $this->options['value']);
        }

        /* Allow Credentials as HTTP-Header */
        $httpResponse->setHeader('Access-Control-Allow-Credentials', 'true');

        /* Allow Content-Type as HTTP-Header */
        $httpResponse->setHeader('Access-Control-Allow-Headers', 'Content-Type');

        /* Allow all regular HTTP-Methods */
        $httpResponse->setHeader('Access-Control-Allow-Methods', ['PUT', 'DELETE', 'POST', 'GET', 'OPTIONS']);
    }
}
