module Commands
    exposing
        ( addCustomer
        , addPosition
        , addProject
        , addSkill
        , addSlogan
        , addTeam
        , addUser
        , changeCustomer
        , changePosition
        , changeProject
        , changeSkill
        , changeSlogan
        , changeTeam
        , changeUser
        , deleteCustomer
        , deletePosition
        , deleteProject
        , deleteSkill
        , deleteSlogan
        , deleteTeam
        , deleteUser
        , fetchCustomers
        , fetchPositions
        , fetchProjects
        , fetchSkills
        , fetchSlogans
        , fetchTeams
        , fetchUsers
        , login
        , logout
        )

import Decoder
    exposing
        ( customerDecoder
        , customersDecoder
        , positionsDecoder
        , projectsDecoder
        , skillDecoder
        , skillsDecoder
        , sloganDecoder
        , slogansDecoder
        , teamsDecoder
        , usersDecoder
        )
import Http
import Models exposing (Customer, Position, Project, Skill, Slogan, Team, User)
import Msgs exposing (Msg)
import RemoteData
import Request
    exposing
        ( addCustomerRequest
        , addPositionRequest
        , addProjectRequest
        , addSkillRequest
        , addSloganRequest
        , addTeamRequest
        , addUserRequest
        , changeCustomerRequest
        , changePositionRequest
        , changeProjectRequest
        , changeSkillRequest
        , changeSloganRequest
        , changeTeamRequest
        , changeUserRequest
        , deleteCustomerRequest
        , deletePositionRequest
        , deleteProjectRequest
        , deleteSkillRequest
        , deleteSloganRequest
        , deleteTeamRequest
        , deleteUserRequest
        , loginRequest
        , logoutRequest
        )
import Urls
    exposing
        ( customersUrl
        , positionsUrl
        , projectsUrl
        , skillsUrl
        , slogansUrl
        , teamsUrl
        , usersUrl
        )


-- Customer


fetchCustomers : Cmd Msg
fetchCustomers =
    Http.get customersUrl customersDecoder
        |> RemoteData.sendRequest
        |> Cmd.map Msgs.OnFetchCustomers


addCustomer : Customer -> Cmd Msg
addCustomer customer =
    addCustomerRequest customer
        |> Http.send Msgs.OnAddCustomer


changeCustomer : Customer -> Customer -> Cmd Msg
changeCustomer newCustomer oldCustomer =
    changeCustomerRequest oldCustomer newCustomer
        |> Http.send Msgs.OnChangeCustomer


deleteCustomer : Customer -> Cmd Msg
deleteCustomer customer =
    deleteCustomerRequest customer
        |> Http.send Msgs.OnDeleteCustomer



-- Position


fetchPositions : Cmd Msg
fetchPositions =
    Http.get positionsUrl positionsDecoder
        |> RemoteData.sendRequest
        |> Cmd.map Msgs.OnFetchPositions


addPosition : Position -> Cmd Msg
addPosition position =
    addPositionRequest position
        |> Http.send Msgs.OnAddPosition


changePosition : Position -> Position -> Cmd Msg
changePosition newPosition oldPosition =
    changePositionRequest oldPosition newPosition
        |> Http.send Msgs.OnChangePosition


deletePosition : Position -> Cmd Msg
deletePosition position =
    deletePositionRequest position
        |> Http.send Msgs.OnDeletePosition



-- Project


fetchProjects : Cmd Msg
fetchProjects =
    Http.get projectsUrl projectsDecoder
        |> RemoteData.sendRequest
        |> Cmd.map Msgs.OnFetchProjects


addProject : Project -> Cmd Msg
addProject project =
    addProjectRequest project
        |> Http.send Msgs.OnAddProject


deleteProject : Project -> Cmd Msg
deleteProject project =
    deleteProjectRequest project
        |> Http.send Msgs.OnDeleteProject


changeProject : Project -> Project -> Cmd Msg
changeProject oldProject newProject =
    changeProjectRequest oldProject newProject
        |> Http.send Msgs.OnChangeProject



-- Skill


fetchSkills : Cmd Msg
fetchSkills =
    Http.get skillsUrl skillsDecoder
        |> RemoteData.sendRequest
        |> Cmd.map Msgs.OnFetchSkills


addSkill : String -> Cmd Msg
addSkill skill =
    addSkillRequest skill
        |> Http.send Msgs.OnAddSkill


changeSkill : String -> String -> Cmd Msg
changeSkill newSkill oldSkill =
    changeSkillRequest oldSkill newSkill
        |> Http.send Msgs.OnChangeSkill


deleteSkill : String -> Cmd Msg
deleteSkill skill =
    deleteSkillRequest skill
        |> Http.send Msgs.OnDeleteSkill



-- Slogan


fetchSlogans : Cmd Msg
fetchSlogans =
    Http.get slogansUrl slogansDecoder
        |> RemoteData.sendRequest
        |> Cmd.map Msgs.OnFetchSlogans


addSlogan : Slogan -> Cmd Msg
addSlogan slogan =
    addSloganRequest slogan
        |> Http.send Msgs.OnAddSlogan


changeSlogan : Slogan -> Slogan -> Cmd Msg
changeSlogan oldSlogan newSlogan =
    changeSloganRequest oldSlogan newSlogan
        |> Http.send Msgs.OnChangeSlogan


deleteSlogan : Slogan -> Cmd Msg
deleteSlogan slogan =
    deleteSloganRequest slogan
        |> Http.send Msgs.OnDeleteSlogan



-- Team


fetchTeams : Cmd Msg
fetchTeams =
    Http.get teamsUrl teamsDecoder
        |> RemoteData.sendRequest
        |> Cmd.map Msgs.OnFetchTeams


addTeam : Team -> Cmd Msg
addTeam team =
    addTeamRequest team
        |> Http.send Msgs.OnAddTeam


deleteTeam : Team -> Cmd Msg
deleteTeam team =
    deleteTeamRequest team
        |> Http.send Msgs.OnDeleteTeam


changeTeam : Team -> Team -> Cmd Msg
changeTeam oldTeam newTeam =
    changeTeamRequest oldTeam newTeam
        |> Http.send Msgs.OnChangeTeam



-- User


fetchUsers : Cmd Msg
fetchUsers =
    Http.get usersUrl usersDecoder
        |> RemoteData.sendRequest
        |> Cmd.map Msgs.OnFetchUsers


addUser : User -> Cmd Msg
addUser user =
    addUserRequest user
        |> Http.send Msgs.OnAddUser


deleteUser : User -> Cmd Msg
deleteUser user =
    deleteUserRequest user
        |> Http.send Msgs.OnDeleteUser


changeUser : User -> User -> Cmd Msg
changeUser oldUser newUser =
    changeUserRequest oldUser newUser
        |> Http.send Msgs.OnChangeUser


login : String -> String -> Cmd Msg
login username password =
    loginRequest username password
        |> Http.send Msgs.OnLogin


logout : Cmd Msg
logout =
    logoutRequest
        |> Http.send Msgs.OnLogout
