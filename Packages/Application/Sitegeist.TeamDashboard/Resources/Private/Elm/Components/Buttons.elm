module Components.Buttons
    exposing
        ( changeButton
        , deleteButton
        , newButton
        )

import Html exposing (Html, button, text)
import Html.Attributes exposing (class, type_)
import Html.Events exposing (onClick)
import Msgs exposing (Msg)


newButton : Html Msg
newButton =
    submitButton "erstellen"


changeButton : Html Msg
changeButton =
    submitButton "speichern"


submitButton : String -> Html Msg
submitButton buttonLabel =
    button [ class "mdl-button primaryButton", type_ "submit" ] [ text buttonLabel ]


deleteButton : Msg -> Html Msg
deleteButton msg =
    button [ class "mdl-button primaryButton deleteButton", onClick msg ] [ text "löschen" ]
