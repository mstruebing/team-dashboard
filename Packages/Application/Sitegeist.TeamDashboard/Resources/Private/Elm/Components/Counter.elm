module Components.Counter
    exposing
        ( counter
        )

import Html exposing (Html, div)
import Html.Attributes exposing (class)
import List
import Models exposing (Model)
import Msgs exposing (Msg)
import Utils exposing (getCustomerCount, getProjectCount, getTeamCount, getUserCount)


counter : Model -> Html Msg
counter model =
    div [ class "counter" ]
        ([ getUserCount model.users (\user -> True)
         , getTeamCount model.teams (\teams -> True)
         , getProjectCount model.projects (\project -> True)
         , getCustomerCount model.customers (\customer -> True)
         ]
            |> List.map (\elem -> div [ class "counter__element" ] [ elem ])
        )
