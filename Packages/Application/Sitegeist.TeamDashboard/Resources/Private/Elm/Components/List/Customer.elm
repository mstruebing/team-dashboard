module Components.List.Customer
    exposing
        ( customerList
        , showSingleCustomer
        )

import Components.Buttons exposing (newButton)
import Html exposing (Html, a, div, form, input, label, p, text)
import Html.Attributes exposing (class, for, href, id, placeholder, required, type_, value)
import Html.Events exposing (onInput, onSubmit)
import List
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Customer, Model)
import Msgs exposing (Msg)
import RemoteData exposing (WebData)
import Utils exposing (executeOnResponseAsList)


customerList : Model -> Html Msg
customerList model =
    div [ class "customer" ] [ createCustomerForm model, maybeCustomerList model.customers ]


createCustomerForm : Model -> Html Msg
createCustomerForm model =
    if model.isAuthenticated then
        form [ class "form", onSubmit Msgs.OnSubmitNewCustomer ]
            [ Textfield.render Msgs.Mdl
                [ 2 ]
                model.mdl
                [ Textfield.label "Kunden name"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newCustomer
                , Options.onInput Msgs.OnUpdateNewCustomer
                ]
                []
            , newButton
            ]
    else
        text ""


maybeCustomerList : WebData (List Customer) -> Html Msg
maybeCustomerList response =
    List.map (\customer -> showSingleCustomer customer)
        |> executeOnResponseAsList response
        |> div [ class "customer__list" ]


showSingleCustomer : Customer -> Html Msg
showSingleCustomer customer =
    div [ class "customer__list__item item__wrapper" ]
        [ p [] [ text "Name: ", a [ href <| "#customers/" ++ String.toLower customer ] [ text customer ] ] ]
