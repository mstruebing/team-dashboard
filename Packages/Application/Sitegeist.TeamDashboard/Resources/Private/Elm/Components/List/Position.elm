module Components.List.Position
    exposing
        ( positionList
        , showSinglePosition
        )

import Components.Buttons exposing (newButton)
import Html exposing (Html, a, div, form, input, label, p, text)
import Html.Attributes exposing (class, for, href, id, placeholder, required, type_, value)
import Html.Events exposing (onInput, onSubmit)
import List exposing (..)
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model, Position)
import Msgs exposing (Msg)
import RemoteData exposing (WebData)
import Utils exposing (executeOnResponseAsList)


positionList : Model -> Html Msg
positionList model =
    div [ class "position" ] [ createPositionForm model, maybePositionList model.positions ]


createPositionForm : Model -> Html Msg
createPositionForm model =
    if model.isAuthenticated then
        form [ class "form", onSubmit Msgs.OnSubmitNewPosition ]
            [ Textfield.render Msgs.Mdl
                [ 2 ]
                model.mdl
                [ Textfield.label "Positions name"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newPosition
                , Options.onInput Msgs.OnUpdateNewPosition
                ]
                []
            , newButton
            ]
    else
        text ""


maybePositionList : WebData (List Position) -> Html Msg
maybePositionList response =
    List.map (\position -> showSinglePosition position)
        |> executeOnResponseAsList response
        |> div [ class "position__list" ]


showSinglePosition : Position -> Html Msg
showSinglePosition position =
    div [ class "postition__list__item item__wrapper" ]
        [ p []
            [ text "Name: "
            , a [ href <| "#positions/" ++ String.toLower position ] [ text position ]
            ]
        ]
