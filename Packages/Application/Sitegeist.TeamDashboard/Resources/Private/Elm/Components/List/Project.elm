module Components.List.Project
    exposing
        ( projectList
        , showSingleProject
        )

import Components.Buttons exposing (newButton)
import Components.Selects exposing (customerSelect, teamSelect, userSelect)
import Html exposing (Html, a, div, form, input, label, p, text)
import Html.Attributes exposing (class, for, href, id, placeholder, required, target, type_, value)
import Html.Events exposing (onInput, onSubmit)
import List exposing (..)
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model, Project)
import Msgs exposing (Msg)
import Utils exposing (executeOnResponseAsList, getUserNameByEmail)


projectList : Model -> Html Msg
projectList model =
    div [ class "project" ] [ createProjectForm model, maybeProjectList model ]


createProjectForm : Model -> Html Msg
createProjectForm model =
    if model.isAuthenticated then
        form [ class "form", onSubmit Msgs.OnSubmitNewProject ]
            [ Textfield.render Msgs.Mdl
                [ 2 ]
                model.mdl
                [ Textfield.label "Projekt Name"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newProject.name
                , Options.onInput <| Msgs.OnUpdateNewProject "name"
                ]
                []
            , Textfield.render Msgs.Mdl
                [ 3 ]
                model.mdl
                [ Textfield.label "Projekt Quojob Nummer"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newProject.quoJobNr
                , Options.onInput <| Msgs.OnUpdateNewProject "quoJobNr"
                ]
                []
            , Textfield.render Msgs.Mdl
                [ 4 ]
                model.mdl
                [ Textfield.label "Projekt Quojob Mandant"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newProject.quoJobClient
                , Options.onInput <| Msgs.OnUpdateNewProject "quoJobClient"
                ]
                []
            , Textfield.render Msgs.Mdl
                [ 5 ]
                model.mdl
                [ Textfield.label "Projekt Gitlab Link"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newProject.gitlab
                , Options.onInput <| Msgs.OnUpdateNewProject "gitlab"
                ]
                []
            , Textfield.render Msgs.Mdl
                [ 6 ]
                model.mdl
                [ Textfield.label "Projekt Redmine Link"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newProject.redmine
                , Options.onInput <| Msgs.OnUpdateNewProject "redmine"
                ]
                []
            , customerSelect model (Msgs.OnUpdateNewProject "customer") model.newProject.customer
            , teamSelect model (Msgs.OnUpdateNewProject "team") model.newProject.team
            , userSelect model (Msgs.OnUpdateNewProject "leadPm") model.newProject.leadPm "Lead PM" 200
            , userSelect model (Msgs.OnUpdateNewProject "leadDev") model.newProject.leadDev "Lead Dev" 210
            , newButton
            ]
    else
        text ""


maybeProjectList : Model -> Html Msg
maybeProjectList model =
    List.map (\position -> showSingleProject model position)
        |> executeOnResponseAsList model.projects
        |> div [ class "project__list" ]


showSingleProject : Model -> Project -> Html Msg
showSingleProject model project =
    div [ class "project__list__item item__wrapper" ]
        [ p [] [ a [ href <| "#projects/" ++ project.quoJobNr ] [ text <| "Name: " ++ project.name ] ]
        , p [] [ text "Team: ", a [ href <| "#teams/" ++ project.team ] [ text project.team ] ]
        , p [] [ text "Kunde: ", a [ href <| "#customers/" ++ project.customer ] [ text project.customer ] ]
        , p [] [ text "Lead PM: ", a [ href <| "#users/" ++ project.leadPm ] [ text <| getUserNameByEmail model project.leadPm ] ]
        , p [] [ text "Lead Dev: ", a [ href <| "#users/" ++ project.leadDev ] [ text <| getUserNameByEmail model project.leadDev ] ]
        , p [] [ text "Gitlab: ", a [ href project.gitlab, target "_blank" ] [ text "Link" ] ]
        , p [] [ text "Redmine: ", a [ href project.redmine, target "_blank" ] [ text "Link" ] ]
        , p [] [ text <| "Quojob Nr: " ++ project.quoJobNr ]
        , p [] [ text <| "Quojob Mandant: " ++ project.quoJobClient ]
        ]
