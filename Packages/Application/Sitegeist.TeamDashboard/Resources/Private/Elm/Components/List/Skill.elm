module Components.List.Skill
    exposing
        ( showSingleSkill
        , skillList
        )

import Components.Buttons exposing (newButton)
import Html exposing (Html, a, div, form, input, label, p, text)
import Html.Attributes exposing (class, for, href, id, placeholder, required, type_, value)
import Html.Events exposing (onInput, onSubmit)
import List exposing (..)
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model, Skill, User)
import Msgs exposing (Msg)
import RemoteData exposing (WebData)
import Utils exposing (executeOnResponseAsList)


skillList : Model -> Html Msg
skillList model =
    div [ class "skill" ] [ createSkillForm model, maybeSkillList model.skills ]


createSkillForm : Model -> Html Msg
createSkillForm model =
    if model.isAuthenticated then
        form [ class "form", onSubmit Msgs.OnSubmitNewSkill ]
            [ Textfield.render Msgs.Mdl
                [ 2 ]
                model.mdl
                [ Textfield.label "Skill bezeichung"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newSkill
                , Options.onInput Msgs.OnUpdateNewSkill
                ]
                []
            , newButton
            ]
    else
        text ""


maybeSkillList : WebData (List Skill) -> Html Msg
maybeSkillList response =
    List.map (\skill -> showSingleSkill skill)
        |> executeOnResponseAsList response
        |> div [ class "skill__list" ]


showSingleSkill : Skill -> Html Msg
showSingleSkill skill =
    div [ class "skill__list__item item__wrapper" ] [ p [] [ a [ href <| "#skills/" ++ String.toLower skill ] [ text skill ] ] ]
