module Components.List.Slogan
    exposing
        ( showSingleSlogan
        , sloganList
        )

import Components.Buttons exposing (changeButton, deleteButton, newButton)
import Html exposing (Html, a, div, form, input, label, p, text)
import Html.Attributes exposing (class, for, href, id, placeholder, required, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import List exposing (..)
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model, Slogan)
import Msgs exposing (Msg)
import Utils exposing (executeOnResponseAsList)


sloganList : Model -> Html Msg
sloganList model =
    if model.isAuthenticated then
        div [ class "slogan" ]
            [ createSloganForm model
            , mabyeSloganList model
            ]
    else
        text ""


createSloganForm : Model -> Html Msg
createSloganForm model =
    form [ class "form", onSubmit Msgs.OnSubmitNewSlogan ]
        [ Textfield.render Msgs.Mdl
            [ 2 ]
            model.mdl
            [ Textfield.label "Slogan"
            , Textfield.floatingLabel
            , Textfield.text_
            , Textfield.value model.newSlogan
            , Options.onInput Msgs.OnUpdateNewSlogan
            ]
            []
        , newButton
        ]


mabyeSloganList : Model -> Html Msg
mabyeSloganList model =
    List.map (\slogan -> showSingleSlogan model slogan)
        |> executeOnResponseAsList model.slogans
        |> div [ class "slogan__list" ]


showSingleSlogan : Model -> Slogan -> Html Msg
showSingleSlogan model slogan =
    div [ class "slogan__list__item item__wrapper" ]
        [ p [] [ a [ href <| "#slogans/" ++ slogan ] [ text <| "Name: " ++ slogan ] ] ]
