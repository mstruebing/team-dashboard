module Components.List.Team
    exposing
        ( showSingleTeam
        , teamList
        )

import Components.Buttons exposing (newButton)
import Components.Selects exposing (userSelect)
import Html exposing (Html, a, div, form, input, label, p, text)
import Html.Attributes exposing (class, for, href, id, placeholder, required, type_, value)
import Html.Events exposing (onInput, onSubmit)
import List exposing (..)
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model, Team, User)
import Msgs exposing (Msg)
import Utils exposing (executeOnResponseAsList, getUserNameByEmail)


teamList : Model -> Html Msg
teamList model =
    div [ class "team" ] [ createTeamForm model, maybeTeamList model ]


createTeamForm : Model -> Html Msg
createTeamForm model =
    if model.isAuthenticated then
        form [ class "form", onSubmit Msgs.OnSubmitNewTeam ]
            [ Textfield.render Msgs.Mdl
                [ 2 ]
                model.mdl
                [ Textfield.label "Team Name"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newTeam.name
                , Options.onInput <| Msgs.OnUpdateNewTeam "name"
                ]
                []
            , Textfield.render Msgs.Mdl
                [ 3 ]
                model.mdl
                [ Textfield.label "Kanban Zeit"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newTeam.kanban
                , Options.onInput <| Msgs.OnUpdateNewTeam "kanban"
                ]
                []
            , userSelect model (Msgs.OnUpdateNewTeam "teamBm") model.newTeam.teamBm "Team BM" 200
            , userSelect model (Msgs.OnUpdateNewTeam "teamCoach") model.newTeam.teamCoach "Team Coach" 210
            , newButton
            ]
    else
        text ""


maybeTeamList : Model -> Html Msg
maybeTeamList model =
    List.map (\team -> showSingleTeam model team)
        |> executeOnResponseAsList model.teams
        |> div [ class "team__list" ]


showSingleTeam : Model -> Team -> Html Msg
showSingleTeam model team =
    div [ class "team__list__item item__wrapper" ]
        [ p [] [ a [ href <| "#teams/" ++ team.name ] [ text <| "Name: " ++ team.name ] ]
        , p [] [ text <| "Kanban: " ++ team.kanban ]
        , p [] [ text "Team BM: ", a [ href <| "#users/" ++ team.teamBm ] [ text (getUserNameByEmail model team.teamBm) ] ]
        , p [] [ text "Team Coach: ", a [ href <| "#users/" ++ team.teamCoach ] [ text (getUserNameByEmail model team.teamCoach) ] ]
        ]
