module Components.List.User
    exposing
        ( showSingleUser
        , userList
        )

import Components.Buttons exposing (newButton)
import Components.Selects exposing (positionSelect, skillSelect, teamSelect)
import Html exposing (Html, a, div, form, input, label, option, p, select, text)
import Html.Attributes exposing (class, for, href, id, maxlength, pattern, placeholder, required, target, title, type_, value)
import Html.Events exposing (onInput, onSubmit)
import List
import Material.Chip as Chip
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model, Position, Team, User)
import Msgs exposing (Msg)
import RemoteData exposing (WebData)
import Utils exposing (executeOnResponseAsList, mailLink)


userList : Model -> Html Msg
userList model =
    div [ class "user" ] [ createUserForm model, maybeUserList model.users ]


createUserForm : Model -> Html Msg
createUserForm model =
    if model.isAuthenticated then
        form [ class "form", onSubmit Msgs.OnSubmitNewUser ]
            [ Textfield.render Msgs.Mdl
                [ 2 ]
                model.mdl
                [ Textfield.label "Name"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newUser.name
                , Options.onInput <| Msgs.OnUpdateNewUser "name"
                ]
                []
            , Textfield.render Msgs.Mdl
                [ 3 ]
                model.mdl
                [ Textfield.label "Kürzel"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newUser.shorthand
                , Options.onInput <| Msgs.OnUpdateNewUser "shorthand"
                ]
                []
            , Textfield.render Msgs.Mdl
                [ 4 ]
                model.mdl
                [ Textfield.label "E-Mail"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newUser.email
                , Options.onInput <| Msgs.OnUpdateNewUser "email"
                ]
                []
            , Textfield.render Msgs.Mdl
                [ 5 ]
                model.mdl
                [ Textfield.label "Durchwahl"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newUser.telephone
                , Options.onInput <| Msgs.OnUpdateNewUser "telephone"
                ]
                []
            , Textfield.render Msgs.Mdl
                [ 6 ]
                model.mdl
                [ Textfield.label "Gitlab"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newUser.gitlab
                , Options.onInput <| Msgs.OnUpdateNewUser "gitlab"
                ]
                []
            , Textfield.render Msgs.Mdl
                [ 7 ]
                model.mdl
                [ Textfield.label "Redmine"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newUser.redmine
                , Options.onInput <| Msgs.OnUpdateNewUser "redmine"
                ]
                []
            , Textfield.render Msgs.Mdl
                [ 8 ]
                model.mdl
                [ Textfield.label "Raum"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newUser.room
                , Options.onInput <| Msgs.OnUpdateNewUser "room"
                ]
                []
            , Textfield.render Msgs.Mdl
                [ 9 ]
                model.mdl
                [ Textfield.label "Arbeitszeit"
                , Textfield.floatingLabel
                , Textfield.text_
                , Textfield.value model.newUser.workingHours
                , Options.onInput <| Msgs.OnUpdateNewUser "workingHours"
                ]
                []
            , positionSelect model (Msgs.OnUpdateNewUser "position") model.newUser.position
            , teamSelect model (Msgs.OnUpdateNewUser "team") model.newUser.team
            , div [ class "form__property" ]
                [ label [ class "form__label", for "skills" ] [ text "Skills:" ]
                , skillSelect model.skills model.newUser.skills Msgs.OnUpdateNewUserSkills
                ]
            , newButton
            ]
    else
        text ""


maybeUserList : WebData (List User) -> Html Msg
maybeUserList response =
    List.map (\user -> showSingleUser user)
        |> executeOnResponseAsList response
        |> div [ class "user__list" ]


showSingleUser : User -> Html Msg
showSingleUser user =
    div [ class "user__list__item item__wrapper" ]
        [ p [] [ a [ href <| "#users/" ++ user.email ] [ text <| "Name: " ++ user.name ] ]
        , p [] [ text <| "Kürzel: " ++ user.shorthand ]
        , p [] [ text <| "Team: ", a [ href <| "#teams/" ++ user.team ] [ text user.team ] ]
        , p []
            [ text "E-Mail: "
            , mailLink user.email
            ]
        , p [] [ text <| "Durchwahl: " ++ user.telephone ]
        , p [] [ text "Gitlab: ", a [ href user.gitlab, target "_blank" ] [ text "Link" ] ]
        , p [] [ text "Redmine: ", a [ href user.redmine, target "_blank" ] [ text "Link" ] ]
        , p [] [ text <| "Position: ", a [ href <| "#positions/" ++ user.position ] [ text user.position ] ]
        , p [] (List.append [ text "Skills: " ] (List.map (\skill -> Chip.span [] [ Chip.content [] [ a [ href <| "#skills/" ++ skill ] [ text skill ] ] ]) <| user.skills))
        , p [] [ text <| "Raum: ", a [ href <| "/Images/raumplan.jpg", target "_blank" ] [ text user.room ] ]
        , p [] [ text <| "Arbeitszeit: " ++ user.workingHours ]
        ]
