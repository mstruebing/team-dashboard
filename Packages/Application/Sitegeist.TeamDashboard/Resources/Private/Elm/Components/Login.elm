module Components.Login
    exposing
        ( login
        )

import Html exposing (Html, button, div, form, input, label, text)
import Html.Attributes exposing (class, for, id, placeholder, type_)
import Html.Events exposing (onClick, onInput, onSubmit)
import Material.Button as Button
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model)
import Msgs exposing (Msg)


login : Model -> Html Msg
login model =
    if model.isAuthenticated then
        div [ class "loginForm" ]
            [ Button.render Msgs.Mdl
                [ 0 ]
                model.mdl
                [ Options.onClick Msgs.Logout ]
                [ text "Logout" ]
            ]
    else
        div [ class "loginForm mui-form" ]
            [ Button.render Msgs.Mdl
                [ 1 ]
                model.mdl
                []
                [ text "Login" ]
            , form [ onSubmit Msgs.Login ]
                [ Textfield.render Msgs.Mdl
                    [ 2 ]
                    model.mdl
                    [ Textfield.label "Username"
                    , Textfield.floatingLabel
                    , Textfield.text_
                    , Textfield.value model.username
                    , Options.onInput Msgs.OnUpdateUsername
                    ]
                    []
                , Textfield.render Msgs.Mdl
                    [ 3 ]
                    model.mdl
                    [ Textfield.label "Password"
                    , Textfield.floatingLabel
                    , Textfield.password
                    , Textfield.value model.password
                    , Options.onInput Msgs.OnUpdatePassword
                    ]
                    []
                , Button.render Msgs.Mdl
                    [ 4 ]
                    model.mdl
                    [ Options.cs "primaryButton" ]
                    [ text "Login" ]
                ]
            ]
