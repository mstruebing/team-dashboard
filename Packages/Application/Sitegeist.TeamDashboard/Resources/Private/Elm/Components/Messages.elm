module Components.Messages
    exposing
        ( messages
        )

import Html exposing (Html, div, text)
import Html.Attributes exposing (class)
import Models exposing (Model)
import Msgs exposing (Msg)


messages : Model -> Html Msg
messages model =
    div [ class "messages" ]
        [ errorMessages model
        , successMessages model
        ]


errorMessages : Model -> Html Msg
errorMessages model =
    if String.length model.errorMessage > 0 then
        div [ class "messages__error" ] [ text model.errorMessage ]
    else
        text ""


successMessages : Model -> Html Msg
successMessages model =
    if String.length model.successMessage > 0 then
        div [ class "messages__success" ] [ text model.successMessage ]
    else
        text ""
