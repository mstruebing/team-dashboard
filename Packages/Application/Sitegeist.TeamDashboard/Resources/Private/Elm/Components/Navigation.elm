module Components.Navigation
    exposing
        ( navigation
        )

import Html exposing (Html, a, li, nav, text, ul)
import Html.Attributes exposing (class, href, placeholder, type_)
import Models exposing (Model, Route(..))
import Msgs exposing (Msg)


navigation : Model -> Html Msg
navigation model =
    nav [ class "navigation" ]
        [ ul [ class "navigation__list" ]
            [ li [ makeClassName "Index" model.route ] [ makeLink "#" "Index" ]
            , li [ makeClassName "User" model.route ] [ makeLink "#users" "Mitarbeiter" ]
            , li [ makeClassName "Project" model.route ] [ makeLink "#projects" "Projekte" ]
            , li [ makeClassName "Customer" model.route ] [ makeLink "#customers" "Kunden" ]
            , li [ makeClassName "Team" model.route ] [ makeLink "#teams" "Teams" ]
            , li [ makeClassName "Position" model.route ] [ makeLink "#positions" "Positionen" ]
            , li [ makeClassName "Skill" model.route ] [ makeLink "#skills" "Skills" ]
            , if model.isAuthenticated then
                li [ makeClassName "Slogan" model.route ] [ makeLink "#slogans" "Slogans" ]
              else
                text ""
            ]
        ]


makeClassName : String -> Route -> Html.Attribute msg
makeClassName routePath currentRoute =
    if String.contains routePath (toString currentRoute) then
        class "navigation__list__item navigation__list__item--active"
    else
        class "navigation__list__item"


makeLink : String -> String -> Html Msg
makeLink targetLocation locationName =
    a [ class "navigation__list__item__link", href targetLocation ] [ text locationName ]
