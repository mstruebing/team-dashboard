module Components.SearchResults
    exposing
        ( searchResults
        )

import Components.List.Customer exposing (showSingleCustomer)
import Components.List.Position exposing (showSinglePosition)
import Components.List.Project exposing (showSingleProject)
import Components.List.Skill exposing (showSingleSkill)
import Components.List.Team exposing (showSingleTeam)
import Components.List.User exposing (showSingleUser)
import Html exposing (Html, a, div, h1, p, text)
import Html.Attributes exposing (class, href, target)
import List exposing (..)
import Models exposing (Customer, Model, Position, Project, Skill, Team, User)
import Msgs exposing (Msg)
import RemoteData exposing (WebData)
import String exposing (isEmpty)
import Utils exposing (executeOnResponse)


searchResults : Model -> Html Msg
searchResults model =
    div [ class "searchResults" ]
        [ maybeUserList model.searchQuery model.users
        , maybeProjectList model
        , maybeCustomerList model.searchQuery model.customers
        , maybeTeamList model
        , maybePositionList model.searchQuery model.positions
        , maybeSkillList model.searchQuery model.skills
        ]


showUserResults : String -> List User -> Html Msg
showUserResults searchQuery users =
    div [ class "searchResults__results" ]
        (h1 [ class "searchResults__headline" ] [ text "Mitarbeiter" ]
            :: (users
                    |> List.filter
                        (\user ->
                            String.contains (String.toLower searchQuery) (String.toLower user.name)
                                || String.contains (String.toLower searchQuery) (String.toLower user.shorthand)
                                || String.contains (String.toLower searchQuery) (String.toLower user.email)
                                || String.contains (String.toLower searchQuery) (String.toLower user.telephone)
                                || String.contains (String.toLower searchQuery) (String.toLower user.position)
                                || String.contains (String.toLower searchQuery) (String.toLower user.team)
                                || String.contains (String.toLower searchQuery) (String.toLower user.room)
                                || List.any
                                    (\skill ->
                                        String.contains (String.toLower searchQuery) (String.toLower skill)
                                    )
                                    user.skills
                        )
                    |> List.map (\user -> showSingleUser user)
               )
        )


maybeUserList : String -> WebData (List User) -> Html Msg
maybeUserList searchQuery response =
    showUserResults searchQuery
        |> executeOnResponse response


showCustomerResults : String -> List Customer -> Html Msg
showCustomerResults searchQuery customers =
    div [ class "searchResults__results" ]
        (h1 [ class "searchResults__headline" ] [ text "Kunden" ]
            :: (customers
                    |> List.filter
                        (\customer -> String.contains (String.toLower searchQuery) (String.toLower customer))
                    |> List.map (\customer -> showSingleCustomer customer)
               )
        )


maybeCustomerList : String -> WebData (List Customer) -> Html Msg
maybeCustomerList searchQuery response =
    showCustomerResults searchQuery
        |> executeOnResponse response



-- Project


showProjectResults : Model -> List Project -> Html Msg
showProjectResults model projects =
    div [ class "searchResults__results" ]
        (h1 [ class "searchResults__headline" ] [ text "Projekte" ]
            :: (projects
                    |> List.filter
                        (\project ->
                            String.contains (String.toLower model.searchQuery) (String.toLower project.name)
                                || String.contains (String.toLower model.searchQuery) (String.toLower project.customer)
                                || String.contains (String.toLower model.searchQuery) (String.toLower project.leadPm)
                                || String.contains (String.toLower model.searchQuery) (String.toLower project.leadDev)
                                || String.contains (String.toLower model.searchQuery) (String.toLower project.team)
                                || String.contains (String.toLower model.searchQuery) (String.toLower project.quoJobNr)
                                || String.contains (String.toLower model.searchQuery) (String.toLower project.quoJobClient)
                        )
                    |> List.map (\project -> showSingleProject model project)
               )
        )


maybeProjectList : Model -> Html Msg
maybeProjectList model =
    showProjectResults model
        |> executeOnResponse model.projects



-- Skill


showSkillResults : String -> List Skill -> Html Msg
showSkillResults searchQuery skills =
    div [ class "searchResults__results" ]
        (h1 [ class "searchResults__headline" ] [ text "Skills" ]
            :: (skills
                    |> List.filter
                        (\skill ->
                            String.contains (String.toLower searchQuery) (String.toLower skill)
                        )
                    |> List.map (\skill -> showSingleSkill skill)
               )
        )


maybeSkillList : String -> WebData (List Skill) -> Html Msg
maybeSkillList searchQuery response =
    showSkillResults searchQuery
        |> executeOnResponse response


showTeamResults : Model -> List Team -> Html Msg
showTeamResults model teams =
    div [ class "searchResults__results" ]
        (h1 [ class "searchResults__headline" ] [ text "Teams" ]
            :: (teams
                    |> List.filter
                        (\team ->
                            String.contains (String.toLower model.searchQuery) (String.toLower team.name)
                                || String.contains (String.toLower model.searchQuery) (String.toLower team.teamBm)
                                || String.contains (String.toLower model.searchQuery) (String.toLower team.teamCoach)
                        )
                    |> List.map (\team -> showSingleTeam model team)
               )
        )


maybeTeamList : Model -> Html Msg
maybeTeamList model =
    showTeamResults model
        |> executeOnResponse model.teams



-- Position


showPositionResults : String -> List Position -> Html Msg
showPositionResults searchQuery positions =
    div [ class "searchResults__results" ]
        (h1 [ class "searchResults__headline" ] [ text "Positionen" ]
            :: (positions
                    |> List.filter
                        (\position ->
                            String.contains (String.toLower searchQuery) (String.toLower position)
                        )
                    |> List.map (\position -> showSinglePosition position)
               )
        )


maybePositionList : String -> WebData (List Position) -> Html Msg
maybePositionList searchQuery response =
    showPositionResults searchQuery
        |> executeOnResponse response
