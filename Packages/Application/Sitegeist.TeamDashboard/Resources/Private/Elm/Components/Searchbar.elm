module Components.Searchbar
    exposing
        ( searchbar
        )

import Html exposing (Html, button, div, input, text)
import Html.Attributes exposing (class, placeholder, type_, value)
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model)
import Msgs exposing (Msg)


-- searchbar component


searchbar : Model -> Html Msg
searchbar model =
    div [ class "searchbar" ]
        [ Textfield.render Msgs.Mdl
            [ 10 ]
            model.mdl
            [ Textfield.label "write here to search..."
            , Textfield.floatingLabel
            , Textfield.text_
            , Options.onInput Msgs.OnUpdateSearchQuery
            , Textfield.value model.searchQuery
            ]
            []
        ]
