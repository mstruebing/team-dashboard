module Components.Selects
    exposing
        ( customerSelect
        , positionSelect
        , skillSelect
        , teamSelect
        , userSelect
        )

import Html exposing (Html, option, select, text)
import Html.Attributes exposing (id, value)
import Material.Dropdown.Item as Item
import Material.Select as Select
import Models exposing (Customer, Model, Position, Skill, Team, User)
import Msgs exposing (Msg)
import MultiSelect
import RemoteData exposing (WebData)
import Utils exposing (executeOnResponse, getUserNameByEmail)


teamSelect : Model -> (String -> Msg) -> String -> Html Msg
teamSelect model msg currentTeam =
    executeOnResponse model.teams
        (\teams ->
            Select.render Msgs.Mdl
                [ 100 ]
                model.mdl
                [ Select.label "Team"
                , Select.floatingLabel
                , Select.below
                , Select.value currentTeam
                ]
                (teams
                    |> List.map
                        (\team ->
                            Select.item
                                [ Item.onSelect (msg team.name)
                                ]
                                [ text team.name
                                ]
                        )
                )
        )


userSelect : Model -> (String -> Msg) -> String -> String -> Int -> Html Msg
userSelect model msg currentUser label id =
    executeOnResponse model.users
        (\users ->
            Select.render Msgs.Mdl
                [ id ]
                model.mdl
                [ Select.label label
                , Select.floatingLabel
                , Select.below
                , Select.value <| getUserNameByEmail model currentUser
                ]
                (users
                    |> List.map
                        (\user ->
                            Select.item
                                [ Item.onSelect (msg user.email)
                                ]
                                [ text user.name
                                ]
                        )
                )
        )


customerSelect : Model -> (String -> Msg) -> String -> Html Msg
customerSelect model msg currentCustomer =
    executeOnResponse model.customers
        (\customers ->
            Select.render Msgs.Mdl
                [ 110 ]
                model.mdl
                [ Select.label "Kunde"
                , Select.floatingLabel
                , Select.below
                , Select.value currentCustomer
                ]
                (customers
                    |> List.map
                        (\customer ->
                            Select.item
                                [ Item.onSelect (msg customer)
                                ]
                                [ text customer
                                ]
                        )
                )
        )


positionSelect : Model -> (String -> Msg) -> String -> Html Msg
positionSelect model msg currentPosition =
    executeOnResponse model.positions
        (\positions ->
            Select.render Msgs.Mdl
                [ 120 ]
                model.mdl
                [ Select.label "Position"
                , Select.floatingLabel
                , Select.below
                , Select.value currentPosition
                ]
                (positions
                    |> List.map
                        (\position ->
                            Select.item
                                [ Item.onSelect (msg position)
                                ]
                                [ text position
                                ]
                        )
                )
        )


skillSelectOptions : List Skill -> (List String -> Msg) -> MultiSelect.Options Msg
skillSelectOptions skills msg =
    let
        defaultOptions =
            MultiSelect.defaultOptions msg
    in
    { defaultOptions
        | items = List.map (\skill -> { value = skill, text = skill, enabled = True }) skills
    }


skillSelect : WebData (List Skill) -> List Skill -> (List String -> Msg) -> Html Msg
skillSelect skillResponse selectedSkills msg =
    executeOnResponse skillResponse
        (\skills ->
            MultiSelect.multiSelect
                (skillSelectOptions skills msg)
                [ id "skills" ]
                selectedSkills
        )
