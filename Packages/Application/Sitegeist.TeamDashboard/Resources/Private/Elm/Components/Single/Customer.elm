module Components.Single.Customer
    exposing
        ( showSingleCustomer
        , singleCustomer
        )

import Components.Buttons exposing (changeButton, deleteButton)
import Html exposing (Html, a, div, form, h1, input, label, li, p, text, ul)
import Html.Attributes exposing (class, for, href, id, placeholder, required, target, type_)
import Html.Events exposing (onClick, onInput, onSubmit)
import List exposing (..)
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Customer, Model, Project)
import Msgs exposing (Msg)
import RemoteData exposing (WebData)
import String
import Utils exposing (executeOnResponse, getProjectCount)


singleCustomer : Model -> String -> Maybe (Html Msg)
singleCustomer model customerName =
    maybeCustomerList model customerName
        |> Just


maybeCustomerList : Model -> String -> Html Msg
maybeCustomerList model customerName =
    executeOnResponse model.customers
        (\customers ->
            customers
                |> List.filter (\customer -> String.toLower customer == String.toLower customerName)
                |> head
                |> (\customer -> showSingleCustomer model customer)
        )


showSingleCustomer : Model -> Maybe Customer -> Html Msg
showSingleCustomer model maybeCustomer =
    case maybeCustomer of
        Just customer ->
            div [ class "item__wrapper item__single" ]
                [ p []
                    [ text "Name: "
                    , a [ href <| "#customers/" ++ String.toLower customer ] [ text customer ]
                    ]
                , showControls model customer
                , p [ class "item__list__description" ]
                    [ getProjectCount model.projects (\project -> project.customer == customer) ]
                , maybeProjectList customer model.projects
                ]

        _ ->
            text ""


maybeProjectList : Customer -> WebData (List Project) -> Html Msg
maybeProjectList customer projectResponse =
    (\projects ->
        ul [ class "item__list" ] <|
            (projects
                |> List.filter (\project -> customer == project.customer)
                |> List.map (\project -> li [] [ a [ href <| "#projects/" ++ project.quoJobNr ] [ text <| "Name: " ++ project.name ] ])
            )
    )
        |> executeOnResponse projectResponse


showControls : Model -> Customer -> Html Msg
showControls model customer =
    if model.isAuthenticated then
        div []
            [ form [ class "form", onSubmit (Msgs.OnSubmitChangeCustomer customer) ]
                [ Textfield.render Msgs.Mdl
                    [ 2 ]
                    model.mdl
                    [ Textfield.label "Neuer Name"
                    , Textfield.floatingLabel
                    , Textfield.text_
                    , Textfield.value model.updatedCustomer
                    , Options.onInput Msgs.OnUpdateChangeCustomer
                    ]
                    []
                , changeButton
                ]
            , deleteButton <| Msgs.OnSubmitDeleteCustomer customer
            ]
    else
        text ""
