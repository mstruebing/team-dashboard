module Components.Single.Position
    exposing
        ( singlePosition
        )

import Components.Buttons exposing (changeButton, deleteButton)
import Html exposing (Html, a, div, form, h1, input, label, li, p, text, ul)
import Html.Attributes exposing (class, for, href, id, placeholder, required, target, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import List exposing (..)
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model, Position, User)
import Msgs exposing (Msg)
import RemoteData exposing (WebData)
import String
import Utils exposing (executeOnResponse, getUserCount)


singlePosition : Model -> String -> Maybe (Html Msg)
singlePosition model label =
    Just <| maybePositionList model label


maybePositionList : Model -> String -> Html Msg
maybePositionList model label =
    executeOnResponse model.positions
        (\positions ->
            positions
                |> List.filter (\position -> String.toLower position == String.toLower label)
                |> head
                |> (\position -> showSinglePosition model position)
        )


showSinglePosition : Model -> Maybe Position -> Html Msg
showSinglePosition model maybePosition =
    case maybePosition of
        Just position ->
            div [ class "item__wrapper item__single" ]
                [ p []
                    [ text "Name: "
                    , a [ href <| "#positions/" ++ String.toLower position ] [ text position ]
                    ]
                , showControls model position
                , p [ class "item__list__description" ]
                    [ getUserCount model.users (\user -> position == user.position) ]
                , maybeUserList position model.users
                ]

        Nothing ->
            div [] [ text "POSITION NOT FOUND" ]


maybeUserList : Position -> WebData (List User) -> Html Msg
maybeUserList position userResponse =
    executeOnResponse userResponse
        (\users ->
            ul [ class "item__list" ] <|
                (users
                    |> List.filter (\user -> user.position == position)
                    |> List.map (\user -> li [] [ a [ href <| "#users/" ++ user.email ] [ text user.name ] ])
                )
        )


showControls : Model -> Position -> Html Msg
showControls model position =
    if model.isAuthenticated then
        div []
            [ form [ class "form", onSubmit (Msgs.OnSubmitChangePosition position) ]
                [ Textfield.render Msgs.Mdl
                    [ 2 ]
                    model.mdl
                    [ Textfield.label "Neuer Name"
                    , Textfield.floatingLabel
                    , Textfield.text_
                    , Textfield.value model.updatedPosition
                    , Options.onInput Msgs.OnUpdateChangePosition
                    ]
                    []
                , changeButton
                ]
            , deleteButton <| Msgs.OnSubmitDeletePosition position
            ]
    else
        text ""
