module Components.Single.Project
    exposing
        ( showSingleProject
        , singleProject
        )

import Components.Buttons exposing (changeButton, deleteButton)
import Components.Selects exposing (customerSelect, teamSelect, userSelect)
import Html exposing (Html, a, div, form, h1, input, label, p, text)
import Html.Attributes exposing (class, for, href, id, placeholder, required, target, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import List exposing (..)
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model, Project)
import Msgs exposing (Msg)
import Utils exposing (executeOnResponse, getUserNameByEmail)


singleProject : Model -> String -> Maybe (Html Msg)
singleProject model quoJobNr =
    Just <| maybeProjectList model quoJobNr


maybeProjectList : Model -> String -> Html Msg
maybeProjectList model quoJobNr =
    executeOnResponse model.projects
        (\projects ->
            projects
                |> List.filter (\project -> project.quoJobNr == quoJobNr)
                |> head
                |> showSingleProject model
        )


showSingleProject : Model -> Maybe Project -> Html Msg
showSingleProject model maybeProject =
    case maybeProject of
        Just project ->
            div [ class "item__wrapper item__single" ]
                [ p [] [ a [ href <| "#projects/" ++ project.quoJobNr ] [ text <| "Name: " ++ project.name ] ]
                , p [] [ text "Team: ", a [ href <| "#teams/" ++ project.team ] [ text project.team ] ]
                , p [] [ text "Kunde: ", a [ href <| "#customers/" ++ project.customer ] [ text project.customer ] ]
                , p [] [ text "Lead PM: ", a [ href <| "#users/" ++ project.leadPm ] [ text <| getUserNameByEmail model project.leadPm ] ]
                , p [] [ text "Lead Dev: ", a [ href <| "#users/" ++ project.leadDev ] [ text <| getUserNameByEmail model project.leadDev ] ]
                , p [] [ text "Gitlab: ", a [ href project.gitlab, target "_blank" ] [ text "Link" ] ]
                , p [] [ text "Redmine: ", a [ href project.redmine, target "_blank" ] [ text "Link" ] ]
                , p [] [ text <| "Quojob Nr: " ++ project.quoJobNr ]
                , p [] [ text <| "Quojob Mandant: " ++ project.quoJobClient ]
                , showControls model project
                ]

        Nothing ->
            div [] [ text "PROJECT NOT FOUND" ]


showControls : Model -> Project -> Html Msg
showControls model project =
    if model.isAuthenticated then
        div [] <|
            [ div []
                [ form [ class "form", onSubmit <| Msgs.OnSubmitChangeProject project ]
                    [ Textfield.render Msgs.Mdl
                        [ 2 ]
                        model.mdl
                        [ Textfield.label "Neuer Name"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedProject.name
                        , Options.onInput <| Msgs.OnUpdateChangeProject "name"
                        ]
                        []
                    , Textfield.render Msgs.Mdl
                        [ 3 ]
                        model.mdl
                        [ Textfield.label "QuoJob Nummer"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedProject.quoJobNr
                        , Options.onInput <| Msgs.OnUpdateChangeProject "quoJobNr"
                        ]
                        []
                    , Textfield.render Msgs.Mdl
                        [ 4 ]
                        model.mdl
                        [ Textfield.label "QuoJob Mandant"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedProject.quoJobClient
                        , Options.onInput <| Msgs.OnUpdateChangeProject "quoJobClient"
                        ]
                        []
                    , Textfield.render Msgs.Mdl
                        [ 5 ]
                        model.mdl
                        [ Textfield.label "Gitlab Link"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedProject.gitlab
                        , Options.onInput <| Msgs.OnUpdateChangeProject "gitlab"
                        ]
                        []
                    , Textfield.render Msgs.Mdl
                        [ 6 ]
                        model.mdl
                        [ Textfield.label "Redmine Link"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedProject.redmine
                        , Options.onInput <| Msgs.OnUpdateChangeProject "redmine"
                        ]
                        []
                    , customerSelect model (Msgs.OnUpdateChangeProject "customer") model.updatedProject.customer
                    , teamSelect model (Msgs.OnUpdateChangeProject "team") model.updatedProject.team
                    , userSelect model (Msgs.OnUpdateChangeProject "leadPm") model.updatedProject.leadPm "Lead PM" 200
                    , userSelect model (Msgs.OnUpdateChangeProject "leadDev") model.updatedProject.leadDev "Lead Dev" 210
                    , changeButton
                    ]
                ]
            , deleteButton <| Msgs.OnSubmitDeleteProject project
            ]
    else
        text ""
