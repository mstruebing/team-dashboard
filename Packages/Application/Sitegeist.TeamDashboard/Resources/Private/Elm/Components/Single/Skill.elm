module Components.Single.Skill
    exposing
        ( singleSkill
        )

import Components.Buttons exposing (changeButton, deleteButton)
import Html exposing (Html, a, div, form, h1, input, label, li, p, text, ul)
import Html.Attributes exposing (class, for, href, id, placeholder, target, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import List exposing (..)
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model, Skill, User)
import Msgs exposing (Msg)
import RemoteData exposing (WebData)
import String
import Utils exposing (executeOnResponse, getUserCount, userLink)


singleSkill : Model -> String -> Maybe (Html Msg)
singleSkill model label =
    Just <| maybeSkillList model label


maybeSkillList : Model -> String -> Html Msg
maybeSkillList model label =
    executeOnResponse model.skills
        (\skills ->
            skills
                |> List.filter (\skill -> String.toLower skill == String.toLower label)
                |> head
                |> (\skill -> showSingleSkill skill model)
        )


showSingleSkill : Maybe Skill -> Model -> Html Msg
showSingleSkill maybeSkill model =
    case maybeSkill of
        Just skill ->
            div [ class "item__wrapper item__single" ]
                [ p []
                    [ text "Name: "
                    , a [ href <| "#skills/" ++ String.toLower skill ] [ text skill ]
                    ]
                , showControls model skill
                , p [ class "item__list__description" ]
                    [ getUserCount model.users (\user -> List.member skill user.skills) ]
                , maybeUserList skill model.users
                ]

        Nothing ->
            div [] [ text "SKILL NOT FOUND" ]


maybeUserList : Skill -> WebData (List User) -> Html Msg
maybeUserList skill userResponse =
    executeOnResponse userResponse
        (\users ->
            ul [ class "item__list" ] <|
                (users
                    |> List.filter (\user -> List.member skill user.skills)
                    |> List.map (\user -> li [] [ userLink user ])
                )
        )


showControls : Model -> Skill -> Html Msg
showControls model skill =
    if model.isAuthenticated then
        div []
            [ form [ class "form", onSubmit (Msgs.OnSubmitChangeSkill skill) ]
                [ Textfield.render Msgs.Mdl
                    [ 2 ]
                    model.mdl
                    [ Textfield.label "Neuer Name"
                    , Textfield.floatingLabel
                    , Textfield.text_
                    , Textfield.value model.updatedSkill
                    , Options.onInput Msgs.OnUpdateChangeSkill
                    ]
                    []
                , changeButton
                ]
            , deleteButton <| Msgs.OnSubmitDeleteSkill skill
            ]
    else
        text ""
