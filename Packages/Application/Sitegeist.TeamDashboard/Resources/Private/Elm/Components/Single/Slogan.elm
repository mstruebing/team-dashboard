module Components.Single.Slogan
    exposing
        ( singleSlogan
        )

import Components.Buttons exposing (changeButton, deleteButton)
import Html exposing (Html, a, div, form, h1, input, label, li, p, text, ul)
import Html.Attributes exposing (class, for, href, id, placeholder, target, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import List exposing (..)
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model, Slogan)
import Msgs exposing (Msg)
import String
import Utils exposing (executeOnResponse, userLink)


singleSlogan : Model -> String -> Maybe (Html Msg)
singleSlogan model text =
    Just <| maybeSloganList model text


maybeSloganList : Model -> String -> Html Msg
maybeSloganList model text =
    executeOnResponse model.slogans
        (\slogans ->
            slogans
                |> List.filter (\slogan -> String.toLower slogan == String.toLower text)
                |> head
                |> (\slogan -> showSingleSlogan slogan model)
        )


showSingleSlogan : Maybe Slogan -> Model -> Html Msg
showSingleSlogan maybeSlogan model =
    case maybeSlogan of
        Just slogan ->
            div [ class "item__wrapper item__single" ]
                [ p []
                    [ text "Name: "
                    , a [ href <| "#skills/" ++ String.toLower slogan ] [ text slogan ]
                    ]
                , showControls model slogan
                ]

        Nothing ->
            div [] [ text "SKILL NOT FOUND" ]


showControls : Model -> Slogan -> Html Msg
showControls model slogan =
    if model.isAuthenticated then
        div []
            [ form [ class "form", onSubmit (Msgs.OnSubmitChangeSlogan slogan) ]
                [ Textfield.render Msgs.Mdl
                    [ 2 ]
                    model.mdl
                    [ Textfield.label "Neuer text"
                    , Textfield.floatingLabel
                    , Textfield.text_
                    , Textfield.value model.updatedSlogan
                    , Options.onInput Msgs.OnUpdateChangeSlogan
                    ]
                    []
                , changeButton
                ]
            , deleteButton <| Msgs.OnSubmitDeleteSlogan slogan
            ]
    else
        text ""
