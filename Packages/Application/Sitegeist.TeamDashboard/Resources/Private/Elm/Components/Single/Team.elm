module Components.Single.Team
    exposing
        ( showSingleTeam
        , singleTeam
        )

import Components.Buttons exposing (changeButton, deleteButton)
import Components.Selects exposing (userSelect)
import Html exposing (Html, a, div, form, h1, input, label, li, p, text, ul)
import Html.Attributes exposing (class, for, href, id, placeholder, target, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import List exposing (..)
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model, Team)
import Msgs exposing (Msg)
import Utils exposing (executeOnResponse, executeOnResponseAsList, getProjectCount, getUserCount, getUserNameByEmail, userLink)


singleTeam : Model -> String -> Maybe (Html Msg)
singleTeam model name =
    Just <| maybeTeamList model name


maybeTeamList : Model -> String -> Html Msg
maybeTeamList model name =
    executeOnResponse model.teams
        (\teams ->
            teams
                |> List.filter (\team -> team.name == name)
                |> head
                |> showSingleTeam model
        )


showSingleTeam : Model -> Maybe Team -> Html Msg
showSingleTeam model maybeTeam =
    case maybeTeam of
        Just team ->
            div [ class "item__wrapper item__single" ]
                [ p [] [ a [ href <| "#teams/" ++ team.name ] [ text <| "Name: " ++ team.name ] ]
                , p [] [ text <| "Kanban: " ++ team.kanban ]
                , p [] [ text "Team BM: ", a [ href <| "#users/" ++ team.teamBm ] [ text <| getUserNameByEmail model team.teamBm ] ]
                , p [] [ text "Team Coach: ", a [ href <| "#users/" ++ team.teamCoach ] [ text <| getUserNameByEmail model team.teamCoach ] ]
                , p [ class "item__list__description" ]
                    [ getUserCount model.users (\user -> user.team == team.name) ]
                , ul [ class "item__list" ] <|
                    executeOnResponseAsList
                        model.users
                        (\users ->
                            users
                                |> List.filter (\user -> user.team == team.name)
                                |> List.map (\user -> li [] [ userLink user ])
                        )
                , p [ class "item__list__description" ]
                    [ getProjectCount model.projects (\project -> project.team == team.name) ]
                , ul [ class "item__list" ] <|
                    executeOnResponseAsList model.projects
                        (\projects ->
                            projects
                                |> List.filter (\project -> project.team == team.name)
                                |> List.map (\project -> li [] [ a [ href <| "#projects/" ++ project.quoJobNr ] [ text <| project.name ++ "(" ++ project.customer ++ ")" ] ])
                        )
                , showControls model team
                ]

        Nothing ->
            div [] [ text "TEAM NOT FOUND" ]


showControls : Model -> Team -> Html Msg
showControls model team =
    if model.isAuthenticated then
        div [] <|
            [ div []
                [ form [ onSubmit <| Msgs.OnSubmitChangeTeam team ]
                    [ Textfield.render Msgs.Mdl
                        [ 2 ]
                        model.mdl
                        [ Textfield.label "Neuer Name"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedTeam.name
                        , Options.onInput <| Msgs.OnUpdateChangeTeam "name"
                        ]
                        []
                    , Textfield.render Msgs.Mdl
                        [ 3 ]
                        model.mdl
                        [ Textfield.label "Kanban"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedTeam.kanban
                        , Options.onInput <| Msgs.OnUpdateChangeTeam "kanban"
                        ]
                        []
                    , userSelect model (Msgs.OnUpdateChangeTeam "teamBm") model.updatedTeam.teamBm "Team BM" 200
                    , userSelect model (Msgs.OnUpdateChangeTeam "teamCoach") model.updatedTeam.teamCoach "Team Coach" 210
                    , changeButton
                    ]
                ]
            , deleteButton <| Msgs.OnSubmitDeleteTeam team
            ]
    else
        text ""
