module Components.Single.User
    exposing
        ( showSingleUser
        , singleUser
        )

import Components.Buttons exposing (changeButton, deleteButton)
import Components.Selects exposing (positionSelect, skillSelect, teamSelect)
import Html exposing (Html, a, div, form, h1, input, label, p, text)
import Html.Attributes exposing (class, for, href, id, maxlength, pattern, placeholder, required, target, title, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import List exposing (..)
import Material.Chip as Chip
import Material.Options as Options
import Material.Textfield as Textfield
import Models exposing (Model, User)
import Msgs exposing (Msg)
import Utils exposing (executeOnResponse, mailLink)


singleUser : Model -> String -> Maybe (Html Msg)
singleUser model userMail =
    Just <| maybeUserList model userMail


maybeUserList : Model -> String -> Html Msg
maybeUserList model userMail =
    executeOnResponse model.users
        (\users ->
            users
                |> List.filter (\user -> user.email == userMail)
                |> head
                |> showSingleUser model
        )


showSingleUser : Model -> Maybe User -> Html Msg
showSingleUser model maybeUser =
    case maybeUser of
        Just user ->
            div [ class "item__wrapper item__single" ]
                [ p [] [ a [ href <| "#users/" ++ user.email ] [ text <| "Name: " ++ user.name ] ]
                , p [] [ text <| "Kürzel: " ++ user.shorthand ]
                , p [] [ text <| "Team: ", a [ href <| "#teams/" ++ user.team ] [ text user.team ] ]
                , p []
                    [ text "E-Mail: "
                    , mailLink user.email
                    ]
                , p [] [ text <| "Durchwahl: " ++ user.telephone ]
                , p [] [ text "Gitlab: ", a [ href user.gitlab, target "_blank" ] [ text "Link" ] ]
                , p [] [ text "Redmine: ", a [ href user.redmine, target "_blank" ] [ text "Link" ] ]
                , p [] [ text <| "Position: ", a [ href <| "#positions/" ++ user.position ] [ text user.position ] ]
                , p [] (List.append [ text "Skills: " ] (List.map (\skill -> Chip.span [] [ Chip.content [] [ a [ href <| "#skills/" ++ skill ] [ text skill ] ] ]) <| user.skills))
                , p [] [ text <| "Raum: ", a [ href <| "/Images/raumplan.jpg", target "_blank" ] [ text user.room ] ]
                , p [] [ text <| "Arbeitszeit: " ++ user.workingHours ]
                , showControls model user
                ]

        Nothing ->
            div [] [ text "USER NOT FOUND" ]


showControls : Model -> User -> Html Msg
showControls model user =
    if model.isAuthenticated then
        div [] <|
            [ div []
                [ form [ class "form", onSubmit <| Msgs.OnSubmitChangeUser user ]
                    [ Textfield.render Msgs.Mdl
                        [ 2 ]
                        model.mdl
                        [ Textfield.label "Name"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedUser.name
                        , Options.onInput <| Msgs.OnUpdateChangeUser "name"
                        ]
                        []
                    , Textfield.render Msgs.Mdl
                        [ 3 ]
                        model.mdl
                        [ Textfield.label "Kürzel"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedUser.shorthand
                        , Options.onInput <| Msgs.OnUpdateChangeUser "shorthand"
                        ]
                        []
                    , Textfield.render Msgs.Mdl
                        [ 4 ]
                        model.mdl
                        [ Textfield.label "E-Mail"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedUser.email
                        , Options.onInput <| Msgs.OnUpdateChangeUser "email"
                        ]
                        []
                    , Textfield.render Msgs.Mdl
                        [ 5 ]
                        model.mdl
                        [ Textfield.label "Durchwahl"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedUser.telephone
                        , Options.onInput <| Msgs.OnUpdateChangeUser "telephone"
                        ]
                        []
                    , Textfield.render Msgs.Mdl
                        [ 6 ]
                        model.mdl
                        [ Textfield.label "Gitlab Link"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedUser.gitlab
                        , Options.onInput <| Msgs.OnUpdateChangeUser "gitlab"
                        ]
                        []
                    , Textfield.render Msgs.Mdl
                        [ 7 ]
                        model.mdl
                        [ Textfield.label "Redmine Link"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedUser.redmine
                        , Options.onInput <| Msgs.OnUpdateChangeUser "redmine"
                        ]
                        []
                    , Textfield.render Msgs.Mdl
                        [ 8 ]
                        model.mdl
                        [ Textfield.label "Raum"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedUser.room
                        , Options.onInput <| Msgs.OnUpdateChangeUser "room"
                        ]
                        []
                    , Textfield.render Msgs.Mdl
                        [ 9 ]
                        model.mdl
                        [ Textfield.label "Arbeitszeit"
                        , Textfield.floatingLabel
                        , Textfield.text_
                        , Textfield.value model.updatedUser.workingHours
                        , Options.onInput <| Msgs.OnUpdateChangeUser "workingHours"
                        ]
                        []
                    , positionSelect model (Msgs.OnUpdateChangeUser "position") model.updatedUser.position
                    , teamSelect model (Msgs.OnUpdateChangeUser "team") model.updatedUser.team
                    , div [ class "form__property" ]
                        [ label [ class "form__label", for "skills" ] [ text "Skills:" ]
                        , skillSelect model.skills model.updatedUser.skills Msgs.OnUpdateChangeUserSkills
                        ]
                    , changeButton
                    ]
                ]
            , deleteButton <| Msgs.OnSubmitDeleteUser user
            ]
    else
        text ""
