module Decoder
    exposing
        ( customerDecoder
        , customersDecoder
        , positionDecoder
        , positionsDecoder
        , projectDecoder
        , projectsDecoder
        , skillDecoder
        , skillsDecoder
        , sloganDecoder
        , slogansDecoder
        , teamDecoder
        , teamsDecoder
        , userDecoder
        , usersDecoder
        )

import Json.Decode as Decode
import Json.Decode.Pipeline exposing (decode, optional, required)
import Models exposing (Customer, Position, Project, Skill, Slogan, Team, User)


skillsDecoder : Decode.Decoder (List Skill)
skillsDecoder =
    Decode.list skillDecoder


skillDecoder : Decode.Decoder Skill
skillDecoder =
    Decode.field "label" Decode.string


positionsDecoder : Decode.Decoder (List Position)
positionsDecoder =
    Decode.list positionDecoder


positionDecoder : Decode.Decoder Position
positionDecoder =
    Decode.field "label" Decode.string


customersDecoder : Decode.Decoder (List Customer)
customersDecoder =
    Decode.list customerDecoder


customerDecoder : Decode.Decoder Customer
customerDecoder =
    Decode.field "name" Decode.string


teamsDecoder : Decode.Decoder (List Team)
teamsDecoder =
    Decode.list teamDecoder


teamDecoder : Decode.Decoder Team
teamDecoder =
    decode Team
        |> required "name" Decode.string
        |> required "kanban" Decode.string
        |> required "teamBm" Decode.string
        |> required "teamCoach" Decode.string


projectsDecoder : Decode.Decoder (List Project)
projectsDecoder =
    Decode.list projectDecoder


projectDecoder : Decode.Decoder Project
projectDecoder =
    decode Project
        |> required "name" Decode.string
        |> required "leadPm" Decode.string
        |> required "leadDev" Decode.string
        |> required "customer" Decode.string
        |> required "team" Decode.string
        |> required "gitlab" Decode.string
        |> required "redmine" Decode.string
        |> required "quoJobNr" Decode.string
        |> required "quoJobClient" Decode.string


usersDecoder : Decode.Decoder (List User)
usersDecoder =
    Decode.list userDecoder


userDecoder : Decode.Decoder User
userDecoder =
    decode User
        |> required "name" Decode.string
        |> required "shorthand" Decode.string
        |> required "email" Decode.string
        |> required "telephone" Decode.string
        |> required "gitlab" Decode.string
        |> required "redmine" Decode.string
        |> optional "position" positionDecoder ""
        |> optional "skills" skillsDecoder []
        |> required "room" Decode.string
        |> required "workingHours" Decode.string
        |> required "team" Decode.string


slogansDecoder : Decode.Decoder (List Slogan)
slogansDecoder =
    Decode.list sloganDecoder


sloganDecoder : Decode.Decoder Slogan
sloganDecoder =
    Decode.field "text" Decode.string
