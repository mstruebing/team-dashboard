module Encoder
    exposing
        ( changeCustomerEncoder
        , changePositionEncoder
        , changeProjectEncoder
        , changeSkillEncoder
        , changeSloganEncoder
        , changeTeamEncoder
        , changeUserEncoder
        , deleteCustomerEncoder
        , deletePositionEncoder
        , deleteProjectEncoder
        , deleteSkillEncoder
        , deleteSloganEncoder
        , deleteTeamEncoder
        , deleteUserEncoder
        , newCustomerEncoder
        , newPositionEncoder
        , newProjectEncoder
        , newSkillEncoder
        , newSloganEncoder
        , newTeamEncoder
        , newUserEncoder
        )

import Json.Encode as Encode
import Models exposing (Customer, Position, Project, Skill, Slogan, Team, User)


-- Slogan


newSloganEncoder : Slogan -> Encode.Value
newSloganEncoder slogan =
    let
        attributes =
            [ ( "slogan"
              , Encode.object
                    [ ( "slogan", Encode.string slogan ) ]
              )
            ]
    in
    Encode.object attributes


changeSloganEncoder : Slogan -> Slogan -> Encode.Value
changeSloganEncoder oldSlogan newSlogan =
    let
        attributes =
            [ ( "slogan"
              , Encode.object
                    [ ( "oldSlogan", Encode.string oldSlogan ), ( "newSlogan", Encode.string newSlogan ) ]
              )
            ]
    in
    Encode.object attributes


deleteSloganEncoder : Slogan -> Encode.Value
deleteSloganEncoder slogan =
    let
        attributes =
            [ ( "slogan"
              , Encode.object
                    [ ( "slogan", Encode.string slogan ) ]
              )
            ]
    in
    Encode.object attributes



-- Skill


newSkillEncoder : Skill -> Encode.Value
newSkillEncoder skill =
    let
        attributes =
            [ ( "skill"
              , Encode.object
                    [ ( "label", Encode.string skill ) ]
              )
            ]
    in
    Encode.object attributes


changeSkillEncoder : Skill -> Skill -> Encode.Value
changeSkillEncoder oldSkill newSkill =
    let
        attributes =
            [ ( "skill"
              , Encode.object
                    [ ( "oldSkill", Encode.string oldSkill ), ( "newSkill", Encode.string newSkill ) ]
              )
            ]
    in
    Encode.object attributes


deleteSkillEncoder : Skill -> Encode.Value
deleteSkillEncoder skill =
    let
        attributes =
            [ ( "skill"
              , Encode.object
                    [ ( "label", Encode.string skill ) ]
              )
            ]
    in
    Encode.object attributes



-- Position


newPositionEncoder : Position -> Encode.Value
newPositionEncoder position =
    let
        attributes =
            [ ( "position"
              , Encode.object
                    [ ( "label", Encode.string position ) ]
              )
            ]
    in
    Encode.object attributes


changePositionEncoder : Position -> Position -> Encode.Value
changePositionEncoder oldPosition newPosition =
    let
        attributes =
            [ ( "position"
              , Encode.object
                    [ ( "oldPosition", Encode.string oldPosition ), ( "newPosition", Encode.string newPosition ) ]
              )
            ]
    in
    Encode.object attributes


deletePositionEncoder : Position -> Encode.Value
deletePositionEncoder position =
    let
        attributes =
            [ ( "position"
              , Encode.object
                    [ ( "label", Encode.string position ) ]
              )
            ]
    in
    Encode.object attributes



-- Customer


newCustomerEncoder : Customer -> Encode.Value
newCustomerEncoder customer =
    let
        attributes =
            [ ( "customer"
              , Encode.object
                    [ ( "name", Encode.string customer ) ]
              )
            ]
    in
    Encode.object attributes


changeCustomerEncoder : Customer -> Customer -> Encode.Value
changeCustomerEncoder oldCustomer newCustomer =
    let
        attributes =
            [ ( "customer"
              , Encode.object
                    [ ( "oldCustomer", Encode.string oldCustomer ), ( "newCustomer", Encode.string newCustomer ) ]
              )
            ]
    in
    Encode.object attributes


deleteCustomerEncoder : Customer -> Encode.Value
deleteCustomerEncoder customer =
    let
        attributes =
            [ ( "customer"
              , Encode.object
                    [ ( "name", Encode.string customer ) ]
              )
            ]
    in
    Encode.object attributes



-- Team


newTeamEncoder : Team -> Encode.Value
newTeamEncoder team =
    let
        attributes =
            [ ( "team"
              , Encode.object
                    [ ( "name", Encode.string team.name )
                    , ( "kanban", Encode.string team.kanban )
                    , ( "teamBm", Encode.string team.teamBm )
                    , ( "teamCoach", Encode.string team.teamCoach )
                    ]
              )
            ]
    in
    Encode.object attributes


deleteTeamEncoder : Team -> Encode.Value
deleteTeamEncoder team =
    let
        attributes =
            [ ( "team"
              , Encode.object
                    [ ( "name", Encode.string team.name )
                    , ( "kanban", Encode.string team.kanban )
                    , ( "teamBm", Encode.string team.teamBm )
                    , ( "teamCoach", Encode.string team.teamCoach )
                    ]
              )
            ]
    in
    Encode.object attributes


changeTeamEncoder : Team -> Team -> Encode.Value
changeTeamEncoder oldTeam newTeam =
    let
        attributes =
            [ ( "team"
              , Encode.object
                    [ ( "oldTeam"
                      , Encode.object
                            [ ( "name", Encode.string oldTeam.name )
                            , ( "kanban", Encode.string oldTeam.kanban )
                            , ( "teamBm", Encode.string oldTeam.teamBm )
                            , ( "teamCoach", Encode.string oldTeam.teamCoach )
                            ]
                      )
                    , ( "newTeam"
                      , Encode.object
                            [ ( "name", Encode.string newTeam.name )
                            , ( "kanban", Encode.string newTeam.kanban )
                            , ( "teamBm", Encode.string newTeam.teamBm )
                            , ( "teamCoach", Encode.string newTeam.teamCoach )
                            ]
                      )
                    ]
              )
            ]
    in
    Encode.object attributes



-- Project


newProjectEncoder : Project -> Encode.Value
newProjectEncoder project =
    let
        attributes =
            [ ( "project"
              , Encode.object
                    [ ( "name", Encode.string project.name )
                    , ( "team", Encode.string project.team )
                    , ( "customer", Encode.string project.customer )
                    , ( "leadPm", Encode.string project.leadPm )
                    , ( "leadDev", Encode.string project.leadDev )
                    , ( "gitlab", Encode.string project.gitlab )
                    , ( "redmine", Encode.string project.redmine )
                    , ( "quoJobNr", Encode.string project.quoJobNr )
                    , ( "quoJobClient", Encode.string project.quoJobClient )
                    ]
              )
            ]
    in
    Encode.object attributes


deleteProjectEncoder : Project -> Encode.Value
deleteProjectEncoder project =
    let
        attributes =
            [ ( "project"
              , Encode.object
                    [ ( "name", Encode.string project.name )
                    , ( "team", Encode.string project.team )
                    , ( "customer", Encode.string project.customer )
                    , ( "leadPm", Encode.string project.leadPm )
                    , ( "leadDev", Encode.string project.leadDev )
                    , ( "gitlab", Encode.string project.gitlab )
                    , ( "redmine", Encode.string project.redmine )
                    , ( "quoJobNr", Encode.string project.quoJobNr )
                    , ( "quoJobClient", Encode.string project.quoJobClient )
                    ]
              )
            ]
    in
    Encode.object attributes


changeProjectEncoder : Project -> Project -> Encode.Value
changeProjectEncoder oldProject newProject =
    let
        attributes =
            [ ( "project"
              , Encode.object
                    [ ( "oldProject"
                      , Encode.object
                            [ ( "name", Encode.string oldProject.name )
                            , ( "customer", Encode.string oldProject.customer )
                            , ( "leadDev", Encode.string oldProject.leadDev )
                            , ( "leadPm", Encode.string oldProject.leadPm )
                            , ( "gitlab", Encode.string oldProject.gitlab )
                            , ( "redmine", Encode.string oldProject.redmine )
                            , ( "quoJobNr", Encode.string oldProject.quoJobNr )
                            , ( "quoJobClient", Encode.string oldProject.quoJobClient )
                            , ( "team", Encode.string oldProject.team )
                            ]
                      )
                    , ( "newProject"
                      , Encode.object
                            [ ( "name", Encode.string newProject.name )
                            , ( "customer", Encode.string newProject.customer )
                            , ( "leadDev", Encode.string newProject.leadDev )
                            , ( "leadPm", Encode.string newProject.leadPm )
                            , ( "gitlab", Encode.string newProject.gitlab )
                            , ( "redmine", Encode.string newProject.redmine )
                            , ( "quoJobNr", Encode.string newProject.quoJobNr )
                            , ( "quoJobClient", Encode.string newProject.quoJobClient )
                            , ( "team", Encode.string newProject.team )
                            ]
                      )
                    ]
              )
            ]
    in
    Encode.object attributes



-- User


newUserEncoder : User -> Encode.Value
newUserEncoder user =
    let
        attributes =
            [ ( "user"
              , Encode.object
                    [ ( "name", Encode.string user.name )
                    , ( "shorthand", Encode.string user.shorthand )
                    , ( "email", Encode.string user.email )
                    , ( "telephone", Encode.string user.telephone )
                    , ( "gitlab", Encode.string user.gitlab )
                    , ( "redmine", Encode.string user.redmine )
                    , ( "position", Encode.string user.position )
                    , ( "skills", Encode.string <| String.join ", " user.skills )
                    , ( "room", Encode.string user.room )
                    , ( "workingHours", Encode.string user.workingHours )
                    , ( "team", Encode.string user.team )
                    ]
              )
            ]
    in
    Encode.object attributes


deleteUserEncoder : User -> Encode.Value
deleteUserEncoder user =
    let
        attributes =
            [ ( "user"
              , Encode.object
                    [ ( "name", Encode.string user.name )
                    , ( "shorthand", Encode.string user.shorthand )
                    , ( "email", Encode.string user.email )
                    , ( "telephone", Encode.string user.telephone )
                    , ( "gitlab", Encode.string user.gitlab )
                    , ( "redmine", Encode.string user.redmine )
                    , ( "position", Encode.string user.position )
                    , ( "skills", Encode.string <| String.join ", " user.skills )
                    , ( "room", Encode.string user.room )
                    , ( "workingHours", Encode.string user.workingHours )
                    , ( "team", Encode.string user.team )
                    ]
              )
            ]
    in
    Encode.object attributes


changeUserEncoder : User -> User -> Encode.Value
changeUserEncoder oldUser newUser =
    let
        attributes =
            [ ( "user"
              , Encode.object
                    [ ( "oldUser"
                      , Encode.object
                            [ ( "name", Encode.string oldUser.name )
                            , ( "email", Encode.string oldUser.email )
                            , ( "gitlab", Encode.string oldUser.gitlab )
                            , ( "position", Encode.string oldUser.position )
                            , ( "redmine", Encode.string oldUser.redmine )
                            , ( "shorthand", Encode.string oldUser.shorthand )
                            , ( "team", Encode.string oldUser.team )
                            , ( "telephone", Encode.string oldUser.telephone )
                            , ( "room", Encode.string oldUser.room )
                            , ( "workingHours", Encode.string oldUser.workingHours )
                            , ( "skills", Encode.string <| String.join ", " oldUser.skills )
                            ]
                      )
                    , ( "newUser"
                      , Encode.object
                            [ ( "name", Encode.string newUser.name )
                            , ( "email", Encode.string newUser.email )
                            , ( "gitlab", Encode.string newUser.gitlab )
                            , ( "position", Encode.string newUser.position )
                            , ( "redmine", Encode.string newUser.redmine )
                            , ( "shorthand", Encode.string newUser.shorthand )
                            , ( "team", Encode.string newUser.team )
                            , ( "telephone", Encode.string newUser.telephone )
                            , ( "room", Encode.string newUser.room )
                            , ( "workingHours", Encode.string newUser.workingHours )
                            , ( "skills", Encode.string <| String.join ", " newUser.skills )
                            ]
                      )
                    ]
              )
            ]
    in
    Encode.object attributes
