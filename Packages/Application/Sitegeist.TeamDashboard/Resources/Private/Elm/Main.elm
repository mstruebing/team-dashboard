module Main
    exposing
        ( main
        )

import Commands
    exposing
        ( fetchCustomers
        , fetchPositions
        , fetchProjects
        , fetchSkills
        , fetchSlogans
        , fetchTeams
        , fetchUsers
        )
import Models exposing (Model, initialModel)
import Msgs exposing (Msg)
import Navigation exposing (Location, program)
import Routing exposing (parseLocation)
import Subscriptions exposing (subscriptions)
import Update exposing (update)
import View exposing (view)


init : Location -> ( Model, Cmd Msg )
init location =
    let
        currentRoute =
            Routing.parseLocation location
    in
    ( initialModel currentRoute
    , Cmd.batch
        [ fetchSkills
        , fetchPositions
        , fetchCustomers
        , fetchTeams
        , fetchProjects
        , fetchUsers
        , fetchSlogans
        ]
    )


main : Program Never Model Msg
main =
    program Msgs.OnLocationChange
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
