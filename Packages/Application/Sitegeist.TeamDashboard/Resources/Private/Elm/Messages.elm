module Messages
    exposing
        ( addMessage
        , changeMessage
        , deleteMessage
        )


addMessage : String -> String
addMessage name =
    name ++ " erfolgreich erstellt"


changeMessage : String -> String
changeMessage name =
    name ++ " erfolgreich geändert"


deleteMessage : String -> String
deleteMessage name =
    name ++ " erfolgreich gelöscht"
