module Models exposing (..)

import Material
import RemoteData exposing (WebData)


type alias Model =
    { route : Route
    , searchQuery : String
    , skills : WebData (List Skill)
    , positions : WebData (List Position)
    , customers : WebData (List Customer)
    , teams : WebData (List Team)
    , users : WebData (List User)
    , projects : WebData (List Project)
    , slogans : WebData (List Slogan)
    , newSkill : Skill
    , newCustomer : Customer
    , newPosition : Position
    , newProject : Project
    , newTeam : Team
    , newUser : User
    , newSlogan : Slogan
    , updatedUser : User
    , updatedSkill : Skill
    , updatedCustomer : Customer
    , updatedPosition : Position
    , updatedSlogan : Slogan
    , updatedProject : Project
    , updatedTeam : Team
    , isAuthenticated : Bool
    , sloganIndex : Int
    , errorMessage : String
    , successMessage : String
    , username : String
    , password : String
    , mdl : Material.Model
    }


initialModel : Route -> Model
initialModel route =
    { route = route
    , searchQuery = ""
    , skills = RemoteData.Loading
    , positions = RemoteData.Loading
    , customers = RemoteData.Loading
    , teams = RemoteData.Loading
    , users = RemoteData.Loading
    , projects = RemoteData.Loading
    , slogans = RemoteData.Loading
    , sloganIndex = 0
    , errorMessage = ""
    , successMessage = ""
    , newSkill = ""
    , newCustomer = ""
    , newTeam =
        { name = ""
        , kanban = ""
        , teamBm = ""
        , teamCoach = ""
        }
    , updatedTeam =
        { name = ""
        , kanban = ""
        , teamBm = ""
        , teamCoach = ""
        }
    , newUser =
        { name = ""
        , shorthand = ""
        , email = ""
        , telephone = ""
        , gitlab = ""
        , redmine = ""
        , position = ""
        , team = ""
        , skills = []
        , room = ""
        , workingHours = ""
        }
    , newProject =
        { name = ""
        , customer = ""
        , gitlab = ""
        , redmine = ""
        , leadPm = ""
        , leadDev = ""
        , team = ""
        , quoJobNr = ""
        , quoJobClient = ""
        }
    , updatedProject =
        { name = ""
        , customer = ""
        , leadPm = ""
        , gitlab = ""
        , redmine = ""
        , leadDev = ""
        , team = ""
        , quoJobNr = ""
        , quoJobClient = ""
        }
    , updatedUser =
        { name = ""
        , shorthand = ""
        , email = ""
        , telephone = ""
        , gitlab = ""
        , redmine = ""
        , position = ""
        , team = ""
        , skills = []
        , room = ""
        , workingHours = ""
        }
    , updatedSlogan = ""
    , newPosition = ""
    , newSlogan = ""
    , updatedSkill = ""
    , updatedCustomer = ""
    , updatedPosition = ""
    , isAuthenticated = False
    , username = ""
    , password = ""
    , mdl = Material.model
    }


type Route
    = IndexRoute
    | UserListRoute
    | UserRoute String
    | TeamListRoute
    | TeamRoute String
    | CustomerListRoute
    | CustomerRoute String
    | ProjectListRoute
    | ProjectRoute String
    | PositionListRoute
    | PositionRoute String
    | SkillListRoute
    | SkillRoute String
    | SloganListRoute
    | SloganRoute String
    | NotFoundRoute


type alias Mdl =
    Material.Model


type alias Skill =
    String


type alias Position =
    String


type alias Customer =
    String


type alias Slogan =
    String


type alias Team =
    { name : String
    , kanban : String
    , teamBm : String
    , teamCoach : String
    }


type alias User =
    { name : String
    , shorthand : String
    , email : String
    , telephone : String
    , gitlab : String
    , redmine : String
    , position : Position
    , skills : List Skill
    , room : String
    , workingHours : String
    , team : String
    }


type alias Project =
    { name : String
    , leadPm : String
    , leadDev : String
    , customer : String
    , team : String
    , gitlab : String
    , redmine : String
    , quoJobNr : String
    , quoJobClient : String
    }


emptySkill : Skill
emptySkill =
    ""


emptySlogan : Slogan
emptySlogan =
    ""


emptyPosition : Position
emptyPosition =
    ""


emptyCustomer : Customer
emptyCustomer =
    ""


emptyTeam : Team
emptyTeam =
    { name = ""
    , kanban = ""
    , teamBm = ""
    , teamCoach = ""
    }


emptyProject : Project
emptyProject =
    { name = ""
    , customer = ""
    , team = ""
    , leadDev = ""
    , leadPm = ""
    , quoJobNr = ""
    , quoJobClient = ""
    , gitlab = ""
    , redmine = ""
    }


emptyUser : User
emptyUser =
    { name = ""
    , shorthand = ""
    , team = ""
    , email = ""
    , telephone = ""
    , gitlab = ""
    , redmine = ""
    , position = ""
    , room = ""
    , workingHours = ""
    , skills = [ "" ]
    }
