module Msgs
    exposing
        ( Msg(..)
        )

import Http
import Material
import Models exposing (Customer, Position, Project, Skill, Slogan, Team, User)
import Navigation exposing (Location)
import RemoteData exposing (WebData)


type Msg
    = NoOp
    | Login
    | Logout
    | OnUpdateSearchQuery String
    | OnLocationChange Location
    | OnUpdateUsername String
    | OnUpdatePassword String
    | OnLogin (Result Http.Error ())
    | OnLogout (Result Http.Error ())
    | Mdl (Material.Msg Msg)
      -- Skills
    | OnAddSkill (Result Http.Error Skill)
    | OnChangeSkill (Result Http.Error Skill)
    | OnDeleteSkill (Result Http.Error Skill)
    | OnFetchSkills (WebData (List Skill))
    | OnSubmitChangeSkill Skill
    | OnSubmitDeleteSkill Skill
    | OnSubmitNewSkill
    | OnUpdateChangeSkill Skill
    | OnUpdateNewSkill Skill
      -- Positions
    | OnAddPosition (Result Http.Error Position)
    | OnChangePosition (Result Http.Error Position)
    | OnDeletePosition (Result Http.Error Position)
    | OnFetchPositions (WebData (List Position))
    | OnSubmitChangePosition Position
    | OnSubmitDeletePosition Position
    | OnSubmitNewPosition
    | OnUpdateChangePosition Position
    | OnUpdateNewPosition Position
      -- Customers
    | OnAddCustomer (Result Http.Error Customer)
    | OnChangeCustomer (Result Http.Error Customer)
    | OnDeleteCustomer (Result Http.Error Customer)
    | OnFetchCustomers (WebData (List Customer))
    | OnSubmitChangeCustomer Customer
    | OnSubmitDeleteCustomer Customer
    | OnSubmitNewCustomer
    | OnUpdateChangeCustomer Customer
    | OnUpdateNewCustomer Customer
      -- Teams
    | OnAddTeam (Result Http.Error Team)
    | OnChangeTeam (Result Http.Error Team)
    | OnDeleteTeam (Result Http.Error Team)
    | OnFetchTeams (WebData (List Team))
    | OnSubmitChangeTeam Team
    | OnSubmitDeleteTeam Team
    | OnSubmitNewTeam
    | OnUpdateChangeTeam String String
    | OnUpdateNewTeam String String
    | PrefillChangeTeam Team
      -- Projects
    | OnAddProject (Result Http.Error Project)
    | OnChangeProject (Result Http.Error Project)
    | OnDeleteProject (Result Http.Error Project)
    | OnFetchProjects (WebData (List Project))
    | OnSubmitChangeProject Project
    | OnSubmitDeleteProject Project
    | OnSubmitNewProject
    | OnUpdateChangeProject String String
    | OnUpdateNewProject String String
      -- Users
    | OnAddUser (Result Http.Error User)
    | OnChangeUser (Result Http.Error User)
    | OnDeleteUser (Result Http.Error User)
    | OnFetchUsers (WebData (List User))
    | OnSubmitChangeUser User
    | OnSubmitDeleteUser User
    | OnSubmitNewUser
    | OnUpdateChangeUser String String
    | OnUpdateChangeUserSkills (List String)
    | OnUpdateNewUser String String
    | OnUpdateNewUserSkills (List String)
      -- Slogans
    | OnAddSlogan (Result Http.Error Slogan)
    | OnChangeSlogan (Result Http.Error Slogan)
    | OnDeleteSlogan (Result Http.Error Slogan)
    | OnFetchSlogans (WebData (List Slogan))
    | OnSubmitChangeSlogan Slogan
    | OnSubmitDeleteSlogan Slogan
    | OnSubmitNewSlogan
    | OnUpdateChangeSlogan Slogan
    | OnUpdateNewSlogan String
    | OnUpdateSloganIndex Int
