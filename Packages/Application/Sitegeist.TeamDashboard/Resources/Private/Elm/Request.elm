module Request
    exposing
        ( addCustomerRequest
        , addPositionRequest
        , addProjectRequest
        , addSkillRequest
        , addSloganRequest
        , addTeamRequest
        , addUserRequest
        , changeCustomerRequest
        , changePositionRequest
        , changeProjectRequest
        , changeSkillRequest
        , changeSloganRequest
        , changeTeamRequest
        , changeUserRequest
        , deleteCustomerRequest
        , deletePositionRequest
        , deleteProjectRequest
        , deleteSkillRequest
        , deleteSloganRequest
        , deleteTeamRequest
        , deleteUserRequest
        , loginRequest
        , logoutRequest
        )

import Decoder
    exposing
        ( customerDecoder
        , positionDecoder
        , projectDecoder
        , skillDecoder
        , sloganDecoder
        , teamDecoder
        , userDecoder
        )
import Encoder
    exposing
        ( changeCustomerEncoder
        , changePositionEncoder
        , changeProjectEncoder
        , changeSkillEncoder
        , changeSloganEncoder
        , changeTeamEncoder
        , changeUserEncoder
        , deleteCustomerEncoder
        , deletePositionEncoder
        , deleteProjectEncoder
        , deleteSkillEncoder
        , deleteSloganEncoder
        , deleteTeamEncoder
        , deleteUserEncoder
        , newCustomerEncoder
        , newPositionEncoder
        , newProjectEncoder
        , newSkillEncoder
        , newSloganEncoder
        , newTeamEncoder
        , newUserEncoder
        )
import Http
import Models
    exposing
        ( Customer
        , Position
        , Project
        , Skill
        , Slogan
        , Team
        , User
        )
import Urls
    exposing
        ( customersUrl
        , loginUrl
        , logoutUrl
        , positionsUrl
        , projectsUrl
        , skillsUrl
        , slogansUrl
        , teamsUrl
        , usersUrl
        )


-- Position


addPositionRequest : Position -> Http.Request String
addPositionRequest position =
    Http.request
        { body = newPositionEncoder position |> Http.jsonBody
        , expect = Http.expectJson positionDecoder
        , headers = []
        , method = "POST"
        , timeout = Nothing
        , url = positionsUrl
        , withCredentials = True
        }


changePositionRequest : Position -> Position -> Http.Request String
changePositionRequest oldPosition newPosition =
    Http.request
        { body = changePositionEncoder oldPosition newPosition |> Http.jsonBody
        , expect = Http.expectJson positionDecoder
        , headers = []
        , method = "PUT"
        , timeout = Nothing
        , url = positionsUrl
        , withCredentials = True
        }


deletePositionRequest : Position -> Http.Request String
deletePositionRequest position =
    Http.request
        { body = deletePositionEncoder position |> Http.jsonBody
        , expect = Http.expectJson positionDecoder
        , headers = []
        , method = "DELETE"
        , timeout = Nothing
        , url = positionsUrl
        , withCredentials = True
        }



-- Slogan


addSloganRequest : Slogan -> Http.Request String
addSloganRequest slogan =
    Http.request
        { body = newSloganEncoder slogan |> Http.jsonBody
        , expect = Http.expectJson sloganDecoder
        , headers = []
        , method = "POST"
        , timeout = Nothing
        , url = slogansUrl
        , withCredentials = True
        }


changeSloganRequest : Slogan -> Slogan -> Http.Request String
changeSloganRequest oldSlogan newSlogan =
    Http.request
        { body = changeSloganEncoder oldSlogan newSlogan |> Http.jsonBody
        , expect = Http.expectJson sloganDecoder
        , headers = []
        , method = "PUT"
        , timeout = Nothing
        , url = slogansUrl
        , withCredentials = True
        }


deleteSloganRequest : Slogan -> Http.Request String
deleteSloganRequest slogan =
    Http.request
        { body = deleteSloganEncoder slogan |> Http.jsonBody
        , expect = Http.expectJson sloganDecoder
        , headers = []
        , method = "DELETE"
        , timeout = Nothing
        , url = slogansUrl
        , withCredentials = True
        }



-- Skill


addSkillRequest : Skill -> Http.Request String
addSkillRequest skill =
    Http.request
        { body = newSkillEncoder skill |> Http.jsonBody
        , expect = Http.expectJson skillDecoder
        , headers = []
        , method = "POST"
        , timeout = Nothing
        , url = skillsUrl
        , withCredentials = True
        }


changeSkillRequest : Skill -> Skill -> Http.Request String
changeSkillRequest oldSkill newSkill =
    Http.request
        { body = changeSkillEncoder oldSkill newSkill |> Http.jsonBody
        , expect = Http.expectJson skillDecoder
        , headers = []
        , method = "PUT"
        , timeout = Nothing
        , url = skillsUrl
        , withCredentials = True
        }


deleteSkillRequest : Skill -> Http.Request String
deleteSkillRequest skill =
    Http.request
        { body = deleteSkillEncoder skill |> Http.jsonBody
        , expect = Http.expectJson skillDecoder
        , headers = []
        , method = "DELETE"
        , timeout = Nothing
        , url = skillsUrl
        , withCredentials = True
        }



-- Customer


addCustomerRequest : Customer -> Http.Request String
addCustomerRequest customer =
    Http.request
        { body = newCustomerEncoder customer |> Http.jsonBody
        , expect = Http.expectJson customerDecoder
        , headers = []
        , method = "POST"
        , timeout = Nothing
        , url = customersUrl
        , withCredentials = True
        }


changeCustomerRequest : Customer -> Customer -> Http.Request String
changeCustomerRequest oldcustomer newcustomer =
    Http.request
        { body = changeCustomerEncoder oldcustomer newcustomer |> Http.jsonBody
        , expect = Http.expectJson customerDecoder
        , headers = []
        , method = "PUT"
        , timeout = Nothing
        , url = customersUrl
        , withCredentials = True
        }


deleteCustomerRequest : Customer -> Http.Request String
deleteCustomerRequest customer =
    Http.request
        { body = deleteCustomerEncoder customer |> Http.jsonBody
        , expect = Http.expectJson customerDecoder
        , headers = []
        , method = "DELETE"
        , timeout = Nothing
        , url = customersUrl
        , withCredentials = True
        }



-- Team


addTeamRequest : Team -> Http.Request Team
addTeamRequest team =
    Http.request
        { body = newTeamEncoder team |> Http.jsonBody
        , expect = Http.expectJson teamDecoder
        , headers = []
        , method = "POST"
        , timeout = Nothing
        , url = teamsUrl
        , withCredentials = True
        }


deleteTeamRequest : Team -> Http.Request Team
deleteTeamRequest team =
    Http.request
        { body = deleteTeamEncoder team |> Http.jsonBody
        , expect = Http.expectJson teamDecoder
        , headers = []
        , method = "DELETE"
        , timeout = Nothing
        , url = teamsUrl
        , withCredentials = True
        }


changeTeamRequest : Team -> Team -> Http.Request Team
changeTeamRequest oldTeam newTeam =
    Http.request
        { body = changeTeamEncoder oldTeam newTeam |> Http.jsonBody
        , expect = Http.expectJson teamDecoder
        , headers = []
        , method = "PUT"
        , timeout = Nothing
        , url = teamsUrl
        , withCredentials = True
        }



-- Project


addProjectRequest : Project -> Http.Request Project
addProjectRequest project =
    Http.request
        { body = newProjectEncoder project |> Http.jsonBody
        , expect = Http.expectJson projectDecoder
        , headers = []
        , method = "POST"
        , timeout = Nothing
        , url = projectsUrl
        , withCredentials = True
        }


deleteProjectRequest : Project -> Http.Request Project
deleteProjectRequest project =
    Http.request
        { body = deleteProjectEncoder project |> Http.jsonBody
        , expect = Http.expectJson projectDecoder
        , headers = []
        , method = "DELETE"
        , timeout = Nothing
        , url = projectsUrl
        , withCredentials = True
        }


changeProjectRequest : Project -> Project -> Http.Request Project
changeProjectRequest oldProject newProject =
    Http.request
        { body = changeProjectEncoder oldProject newProject |> Http.jsonBody
        , expect = Http.expectJson projectDecoder
        , headers = []
        , method = "PUT"
        , timeout = Nothing
        , url = projectsUrl
        , withCredentials = True
        }



-- User


addUserRequest : User -> Http.Request User
addUserRequest user =
    Http.request
        { body = newUserEncoder user |> Http.jsonBody
        , expect = Http.expectJson userDecoder
        , headers = []
        , method = "POST"
        , timeout = Nothing
        , url = usersUrl
        , withCredentials = True
        }


deleteUserRequest : User -> Http.Request User
deleteUserRequest user =
    Http.request
        { body = deleteUserEncoder user |> Http.jsonBody
        , expect = Http.expectJson userDecoder
        , headers = []
        , method = "DELETE"
        , timeout = Nothing
        , url = usersUrl
        , withCredentials = True
        }


changeUserRequest : User -> User -> Http.Request User
changeUserRequest oldUser newUser =
    Http.request
        { body = changeUserEncoder oldUser newUser |> Http.jsonBody
        , expect = Http.expectJson userDecoder
        , headers = []
        , method = "PUT"
        , timeout = Nothing
        , url = usersUrl
        , withCredentials = True
        }



-- Authentication


usernameFieldName : String
usernameFieldName =
    "__authentication[Neos][Flow][Security][Authentication][Token][UsernamePassword][username]"


passwordFieldName : String
passwordFieldName =
    "__authentication[Neos][Flow][Security][Authentication][Token][UsernamePassword][password]"


loginRequest : String -> String -> Http.Request ()
loginRequest username password =
    Http.request
        { body =
            Http.stringBody
                "application/x-www-form-urlencoded"
                (usernameFieldName ++ "=" ++ username ++ "&" ++ passwordFieldName ++ "=" ++ password)
        , expect =
            Http.expectStringResponse
                (\response ->
                    if response.status.code == 204 then
                        Ok ()
                    else
                        Err response.body
                )
        , headers = []
        , method = "POST"
        , timeout = Nothing
        , url = loginUrl
        , withCredentials = True
        }


logoutRequest : Http.Request ()
logoutRequest =
    Http.request
        { body = Http.stringBody "" ""
        , expect =
            Http.expectStringResponse
                (\response ->
                    if response.status.code == 204 then
                        Ok ()
                    else
                        Err response.body
                )
        , headers = []
        , method = "POST"
        , timeout = Nothing
        , url = logoutUrl
        , withCredentials = True
        }
