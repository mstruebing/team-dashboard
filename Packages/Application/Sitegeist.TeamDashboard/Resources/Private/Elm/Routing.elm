module Routing
    exposing
        ( parseLocation
        )

import Models exposing (Route(..))
import Navigation exposing (Location)
import UrlParser exposing (..)


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map IndexRoute top
        , map UserListRoute (s "users")
        , map UserRoute (s "users" </> string)
        , map ProjectListRoute (s "projects")
        , map ProjectRoute (s "projects" </> string)
        , map CustomerListRoute (s "customers")
        , map CustomerRoute (s "customers" </> string)
        , map TeamListRoute (s "teams")
        , map TeamRoute (s "teams" </> string)
        , map PositionListRoute (s "positions")
        , map PositionRoute (s "positions" </> string)
        , map SkillListRoute (s "skills")
        , map SkillRoute (s "skills" </> string)
        , map SloganListRoute (s "slogans")
        , map SloganRoute (s "slogans" </> string)
        ]


parseLocation : Location -> Route
parseLocation location =
    case parseHash matchers location of
        Just route ->
            route

        Nothing ->
            NotFoundRoute
