module Subscriptions
    exposing
        ( subscriptions
        )

import Material
import Material.Select as Select
import Models exposing (Model)
import Msgs exposing (Msg)


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Select.subs Msgs.Mdl model.mdl
        , Material.subscriptions Msgs.Mdl model
        ]
