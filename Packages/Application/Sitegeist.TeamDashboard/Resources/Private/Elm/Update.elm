module Update
    exposing
        ( update
        )

import Commands
    exposing
        ( addCustomer
        , addPosition
        , addProject
        , addSkill
        , addSlogan
        , addTeam
        , addUser
        , changeCustomer
        , changePosition
        , changeProject
        , changeSkill
        , changeSlogan
        , changeTeam
        , changeUser
        , deleteCustomer
        , deletePosition
        , deleteProject
        , deleteSkill
        , deleteSlogan
        , deleteTeam
        , deleteUser
        , fetchCustomers
        , fetchPositions
        , fetchProjects
        , fetchSkills
        , fetchSlogans
        , fetchTeams
        , fetchUsers
        , login
        , logout
        )
import Material
import Messages exposing (addMessage, changeMessage, deleteMessage)
import Models
    exposing
        ( Customer
        , Model
        , Position
        , Project
        , Route(..)
        , Skill
        , Slogan
        , Team
        , User
        , emptyCustomer
        , emptyPosition
        , emptyProject
        , emptySkill
        , emptySlogan
        , emptyTeam
        , emptyUser
        )
import Msgs exposing (Msg)
import Random
import RemoteData
import Routing exposing (parseLocation)
import String exposing (toLower)
import Utils exposing (normalizeSKill, normalizeShorthand)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Msgs.NoOp ->
            ( model, Cmd.none )

        Msgs.OnUpdateSearchQuery input ->
            ( { model | searchQuery = input }, Cmd.none )

        Msgs.OnLocationChange location ->
            let
                newRoute =
                    parseLocation location

                adjustedModel =
                    resetModel model newRoute

                newSloganIndex =
                    case model.slogans of
                        RemoteData.Success slogans ->
                            Random.generate Msgs.OnUpdateSloganIndex (Random.int 0 <| List.length slogans - 1)

                        _ ->
                            Random.generate Msgs.OnUpdateSloganIndex (Random.int 0 0)
            in
            ( { adjustedModel | route = newRoute }, Cmd.batch [ newSloganIndex ] )

        Msgs.Mdl msg_ ->
            Material.update Msgs.Mdl msg_ model

        -- Authentication
        Msgs.OnUpdateUsername input ->
            ( { model | username = input }, Cmd.none )

        Msgs.OnUpdatePassword input ->
            ( { model | password = input }, Cmd.none )

        Msgs.Login ->
            ( model, Cmd.batch [ login model.username model.password ] )

        Msgs.OnLogin (Ok _) ->
            ( { model | isAuthenticated = True, successMessage = "Erfolgreich eingeloggt" }, Cmd.none )

        Msgs.OnLogin (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.Logout ->
            ( model, Cmd.batch [ logout ] )

        Msgs.OnLogout (Ok _) ->
            ( { model | isAuthenticated = False, successMessage = "Erfolgreich ausgeloggt" }, Cmd.none )

        Msgs.OnLogout (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        -- Skills
        Msgs.OnSubmitDeleteSkill skill ->
            ( model, Cmd.batch [ deleteSkill skill ] )

        Msgs.OnAddSkill (Ok skill) ->
            ( addModel model skill, Cmd.batch [ fetchSkills ] )

        Msgs.OnAddSkill (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnChangeSkill (Ok skill) ->
            ( changeModel model SkillListRoute skill, Cmd.batch [ fetchSkills ] )

        Msgs.OnChangeSkill (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnDeleteSkill (Ok skill) ->
            ( deleteModel model SkillListRoute skill, Cmd.batch [ fetchSkills ] )

        Msgs.OnDeleteSkill (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnFetchSkills response ->
            ( { model | skills = response }, Cmd.none )

        Msgs.OnSubmitChangeSkill oldSkill ->
            ( model, Cmd.batch [ changeSkill model.updatedSkill oldSkill, fetchSkills ] )

        Msgs.OnSubmitNewSkill ->
            ( model, Cmd.batch [ addSkill model.newSkill, fetchSkills ] )

        Msgs.OnUpdateChangeSkill skill ->
            ( { model | updatedSkill = normalizeSKill skill }, Cmd.none )

        Msgs.OnUpdateNewSkill skill ->
            ( { model | newSkill = normalizeSKill skill }, Cmd.none )

        -- Positions
        Msgs.OnSubmitDeletePosition position ->
            ( model, Cmd.batch [ deletePosition position ] )

        Msgs.OnAddPosition (Ok position) ->
            ( addModel model position, Cmd.batch [ fetchPositions ] )

        Msgs.OnAddPosition (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnChangePosition (Ok position) ->
            ( changeModel model PositionListRoute position, Cmd.batch [ fetchPositions ] )

        Msgs.OnChangePosition (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnDeletePosition (Ok position) ->
            ( deleteModel model PositionListRoute position, Cmd.batch [ fetchPositions ] )

        Msgs.OnDeletePosition (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnFetchPositions response ->
            ( { model | positions = response }, Cmd.none )

        Msgs.OnSubmitChangePosition oldPosition ->
            ( model, Cmd.batch [ changePosition model.updatedPosition oldPosition, fetchPositions ] )

        Msgs.OnSubmitNewPosition ->
            ( { model | newPosition = "" }, Cmd.batch [ addPosition model.newPosition, fetchPositions ] )

        Msgs.OnUpdateChangePosition position ->
            ( { model | updatedPosition = position }, Cmd.none )

        Msgs.OnUpdateNewPosition position ->
            ( { model | newPosition = position }, Cmd.none )

        -- Customers
        Msgs.OnSubmitDeleteCustomer customer ->
            ( model, Cmd.batch [ deleteCustomer customer ] )

        Msgs.OnAddCustomer (Ok customer) ->
            ( addModel model customer, Cmd.batch [ fetchCustomers ] )

        Msgs.OnAddCustomer (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnChangeCustomer (Ok customer) ->
            ( changeModel model CustomerListRoute customer, Cmd.batch [ fetchCustomers ] )

        Msgs.OnChangeCustomer (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnDeleteCustomer (Ok customer) ->
            ( deleteModel model CustomerListRoute customer, Cmd.batch [ fetchCustomers ] )

        Msgs.OnDeleteCustomer (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnFetchCustomers response ->
            ( { model | customers = response }, Cmd.none )

        Msgs.OnSubmitChangeCustomer oldCustomer ->
            ( model, Cmd.batch [ changeCustomer model.updatedCustomer oldCustomer, fetchCustomers ] )

        Msgs.OnSubmitNewCustomer ->
            ( model, Cmd.batch [ addCustomer model.newCustomer, fetchCustomers ] )

        Msgs.OnUpdateChangeCustomer customer ->
            ( { model | updatedCustomer = customer }, Cmd.none )

        Msgs.OnUpdateNewCustomer customer ->
            ( { model | newCustomer = customer }, Cmd.none )

        -- teams
        Msgs.OnFetchTeams response ->
            ( { model | teams = response }, Cmd.none )

        Msgs.OnUpdateNewTeam attribute value ->
            let
                oldTeam =
                    model.newTeam

                updatedTeam =
                    case attribute of
                        "name" ->
                            { oldTeam | name = value }

                        "kanban" ->
                            { oldTeam | kanban = value }

                        "teamBm" ->
                            { oldTeam | teamBm = value }

                        "teamCoach" ->
                            { oldTeam | teamCoach = value }

                        _ ->
                            oldTeam
            in
            ( { model | newTeam = updatedTeam }, Cmd.none )

        Msgs.OnUpdateChangeTeam attribute value ->
            let
                team =
                    model.updatedTeam

                changedTeam =
                    case attribute of
                        "name" ->
                            { team | name = value }

                        "kanban" ->
                            { team | kanban = value }

                        "teamBm" ->
                            { team | teamBm = value }

                        "teamCoach" ->
                            { team | teamCoach = value }

                        _ ->
                            team
            in
            ( { model | updatedTeam = changedTeam }, Cmd.none )

        Msgs.OnSubmitChangeTeam team ->
            ( model, Cmd.batch [ changeTeam team model.updatedTeam ] )

        Msgs.OnChangeTeam (Ok team) ->
            ( changeModel model TeamListRoute team.name, Cmd.batch [ fetchTeams ] )

        Msgs.OnChangeTeam (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnSubmitNewTeam ->
            ( model, Cmd.batch [ addTeam model.newTeam, fetchTeams ] )

        Msgs.OnAddTeam (Ok team) ->
            ( addModel model team.name, Cmd.batch [ fetchTeams ] )

        Msgs.OnAddTeam (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnSubmitDeleteTeam team ->
            ( model, Cmd.batch [ deleteTeam team ] )

        Msgs.OnDeleteTeam (Ok team) ->
            ( deleteModel model TeamListRoute team.name, Cmd.batch [ fetchTeams ] )

        Msgs.OnDeleteTeam (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.PrefillChangeTeam team ->
            ( { model | updatedTeam = team }, Cmd.none )

        -- projects
        Msgs.OnFetchProjects response ->
            ( { model | projects = response }, Cmd.none )

        Msgs.OnAddProject (Ok project) ->
            ( addModel model project.name, Cmd.batch [ fetchProjects ] )

        Msgs.OnAddProject (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnChangeProject (Ok project) ->
            ( changeModel model ProjectListRoute project.name, Cmd.batch [ fetchProjects ] )

        Msgs.OnChangeProject (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnSubmitNewProject ->
            ( model, Cmd.batch [ addProject model.newProject, fetchProjects ] )

        Msgs.OnSubmitChangeProject project ->
            ( model, Cmd.batch [ changeProject project model.updatedProject ] )

        Msgs.OnUpdateNewProject attribute value ->
            let
                oldProject =
                    model.newProject

                updatedProject =
                    case attribute of
                        "name" ->
                            { oldProject | name = value }

                        "customer" ->
                            { oldProject | customer = value }

                        "leadPm" ->
                            { oldProject | leadPm = value }

                        "leadDev" ->
                            { oldProject | leadDev = value }

                        "team" ->
                            { oldProject | team = value }

                        "quoJobNr" ->
                            { oldProject | quoJobNr = value }

                        "quoJobClient" ->
                            { oldProject | quoJobClient = value }

                        "gitlab" ->
                            { oldProject | gitlab = value }

                        "redmine" ->
                            { oldProject | redmine = value }

                        _ ->
                            oldProject
            in
            ( { model | newProject = updatedProject }, Cmd.none )

        Msgs.OnUpdateChangeProject attribute value ->
            let
                oldProject =
                    model.updatedProject

                updatedProject =
                    case attribute of
                        "name" ->
                            { oldProject | name = value }

                        "customer" ->
                            { oldProject | customer = value }

                        "leadPm" ->
                            { oldProject | leadPm = value }

                        "leadDev" ->
                            { oldProject | leadDev = value }

                        "team" ->
                            { oldProject | team = value }

                        "quoJobNr" ->
                            { oldProject | quoJobNr = value }

                        "quoJobClient" ->
                            { oldProject | quoJobClient = value }

                        "gitlab" ->
                            { oldProject | gitlab = value }

                        "redmine" ->
                            { oldProject | redmine = value }

                        _ ->
                            oldProject
            in
            ( { model | updatedProject = updatedProject }, Cmd.none )

        Msgs.OnSubmitDeleteProject project ->
            ( model, Cmd.batch [ deleteProject project ] )

        Msgs.OnDeleteProject (Ok project) ->
            ( deleteModel model ProjectListRoute project.name, Cmd.batch [ fetchProjects ] )

        Msgs.OnDeleteProject (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        -- users
        Msgs.OnFetchUsers response ->
            ( { model | users = response }, Cmd.none )

        Msgs.OnAddUser (Ok user) ->
            ( addModel model user.name, Cmd.batch [ fetchUsers ] )

        Msgs.OnAddUser (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnSubmitDeleteUser user ->
            ( model, Cmd.batch [ deleteUser user ] )

        Msgs.OnDeleteUser (Ok user) ->
            ( deleteModel model UserListRoute user.name, Cmd.batch [ fetchUsers ] )

        Msgs.OnDeleteUser (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnSubmitNewUser ->
            ( model, Cmd.batch [ addUser model.newUser, fetchUsers ] )

        Msgs.OnSubmitChangeUser user ->
            ( model, Cmd.batch [ changeUser user model.updatedUser, fetchUsers ] )

        Msgs.OnUpdateNewUser attribute value ->
            let
                oldUser =
                    model.newUser

                updatedUser =
                    case attribute of
                        "name" ->
                            { oldUser | name = value }

                        "shorthand" ->
                            { oldUser | shorthand = normalizeShorthand value }

                        "email" ->
                            { oldUser | email = value }

                        "telephone" ->
                            { oldUser | telephone = value }

                        "gitlab" ->
                            { oldUser | gitlab = value }

                        "redmine" ->
                            { oldUser | redmine = value }

                        "position" ->
                            { oldUser | position = value }

                        "team" ->
                            { oldUser | team = value }

                        "room" ->
                            { oldUser | room = value }

                        "workingHours" ->
                            { oldUser | workingHours = value }

                        _ ->
                            oldUser
            in
            ( { model | newUser = updatedUser }, Cmd.none )

        Msgs.OnUpdateNewUserSkills skills ->
            let
                user =
                    model.newUser

                updatedUser =
                    { user | skills = skills }
            in
            ( { model | newUser = updatedUser }, Cmd.none )

        Msgs.OnUpdateChangeUser attribute value ->
            let
                changedUser =
                    model.updatedUser

                updatedUser =
                    case attribute of
                        "name" ->
                            { changedUser | name = value }

                        "shorthand" ->
                            { changedUser | shorthand = normalizeShorthand value }

                        "email" ->
                            { changedUser | email = value }

                        "telephone" ->
                            { changedUser | telephone = value }

                        "gitlab" ->
                            { changedUser | gitlab = value }

                        "redmine" ->
                            { changedUser | redmine = value }

                        "position" ->
                            { changedUser | position = value }

                        "team" ->
                            { changedUser | team = value }

                        "skills" ->
                            { changedUser | skills = String.split ", " value }

                        "room" ->
                            { changedUser | room = value }

                        "workingHours" ->
                            { changedUser | workingHours = value }

                        _ ->
                            changedUser
            in
            ( { model | updatedUser = updatedUser }, Cmd.none )

        Msgs.OnUpdateChangeUserSkills skills ->
            let
                user =
                    model.updatedUser

                updatedUser =
                    { user | skills = skills }
            in
            ( { model | updatedUser = updatedUser }, Cmd.none )

        Msgs.OnChangeUser (Ok user) ->
            ( changeModel model UserListRoute user.name, Cmd.batch [ fetchUsers ] )

        Msgs.OnChangeUser (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        -- Slogans
        Msgs.OnSubmitDeleteSlogan slogan ->
            ( model, Cmd.batch [ deleteSlogan slogan ] )

        Msgs.OnAddSlogan (Ok slogan) ->
            ( addModel model slogan, Cmd.batch [ fetchSlogans ] )

        Msgs.OnAddSlogan (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnChangeSlogan (Ok slogan) ->
            ( changeModel model SloganListRoute slogan, Cmd.batch [ fetchSlogans ] )

        Msgs.OnChangeSlogan (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnDeleteSlogan (Ok slogan) ->
            ( deleteModel model SloganListRoute slogan, Cmd.batch [ fetchSlogans ] )

        Msgs.OnDeleteSlogan (Err err) ->
            ( { model | errorMessage = toString err }, Cmd.none )

        Msgs.OnFetchSlogans response ->
            ( { model | slogans = response }, Cmd.none )

        Msgs.OnSubmitNewSlogan ->
            ( model, addSlogan model.newSlogan )

        Msgs.OnSubmitChangeSlogan slogan ->
            ( model, Cmd.batch [ changeSlogan slogan model.updatedSlogan ] )

        Msgs.OnUpdateChangeSlogan slogan ->
            ( { model | updatedSlogan = slogan }, Cmd.none )

        Msgs.OnUpdateNewSlogan slogan ->
            ( { model | newSlogan = slogan }, Cmd.none )

        Msgs.OnUpdateSloganIndex index ->
            ( { model | sloganIndex = index }, Cmd.none )


resetModel : Model -> Route -> Model
resetModel model route =
    { model
        | route = route
        , newCustomer = ""
        , searchQuery = ""
        , newPosition = emptyPosition
        , newSlogan = emptySlogan
        , newProject = emptyProject
        , newSkill = emptySkill
        , newTeam = emptyTeam
        , newUser = emptyUser
        , updatedUser = fillUpdatedUser model route
        , updatedCustomer = fillUpdatedCustomer model route
        , updatedPosition = fillUpdatedPosition model route
        , updatedProject = fillUpdatedProject model route
        , updatedSkill = fillUpdatedSkill model route
        , updatedSlogan = fillUpdatedSlogan model route
        , updatedTeam = fillUpdatedTeam model route
        , errorMessage = ""
        , successMessage = ""
        , username = ""
        , password = ""
        , mdl = Material.model
    }


addModel : Model -> String -> Model
addModel model name =
    let
        newModel =
            resetModel model model.route
    in
    { newModel | successMessage = addMessage name }


changeModel : Model -> Route -> String -> Model
changeModel model route name =
    let
        newModel =
            resetModel model route
    in
    { newModel | successMessage = changeMessage name }


deleteModel : Model -> Route -> String -> Model
deleteModel model route name =
    let
        newModel =
            resetModel model route
    in
    { newModel | successMessage = deleteMessage name }


fillUpdatedSkill : Model -> Route -> Skill
fillUpdatedSkill model route =
    case route of
        SkillRoute skill ->
            case model.skills of
                RemoteData.Success skills ->
                    if List.member skill skills then
                        skill
                    else
                        emptySkill

                _ ->
                    emptySkill

        _ ->
            emptySkill


fillUpdatedSlogan : Model -> Route -> Slogan
fillUpdatedSlogan model route =
    case route of
        SloganRoute slogan ->
            case model.slogans of
                RemoteData.Success slogans ->
                    slogans
                        |> List.filter (\elem -> String.toLower elem == String.toLower slogan)
                        |> List.head
                        |> Maybe.withDefault emptySlogan

                _ ->
                    emptySlogan

        _ ->
            emptySlogan


fillUpdatedPosition : Model -> Route -> Position
fillUpdatedPosition model route =
    case route of
        PositionRoute position ->
            case model.positions of
                RemoteData.Success positions ->
                    positions
                        |> List.filter (\elem -> String.toLower elem == String.toLower position)
                        |> List.head
                        |> Maybe.withDefault emptyPosition

                _ ->
                    emptyPosition

        _ ->
            emptyPosition


fillUpdatedCustomer : Model -> Route -> Customer
fillUpdatedCustomer model route =
    case route of
        CustomerRoute customer ->
            case model.customers of
                RemoteData.Success customers ->
                    customers
                        |> List.filter (\elem -> String.toLower elem == String.toLower customer)
                        |> List.head
                        |> Maybe.withDefault emptyCustomer

                _ ->
                    emptyCustomer

        _ ->
            emptyCustomer


fillUpdatedTeam : Model -> Route -> Team
fillUpdatedTeam model route =
    case route of
        TeamRoute teamRoute ->
            case model.teams of
                RemoteData.Success teams ->
                    teams
                        |> List.filter (\team -> String.toLower teamRoute == String.toLower team.name)
                        |> List.head
                        |> Maybe.withDefault emptyTeam

                _ ->
                    emptyTeam

        _ ->
            emptyTeam


fillUpdatedProject : Model -> Route -> Project
fillUpdatedProject model route =
    case route of
        ProjectRoute projectRoute ->
            case model.projects of
                RemoteData.Success projects ->
                    projects
                        |> List.filter (\project -> String.toLower projectRoute == String.toLower project.quoJobNr)
                        |> List.head
                        |> Maybe.withDefault emptyProject

                _ ->
                    emptyProject

        _ ->
            emptyProject


fillUpdatedUser : Model -> Route -> User
fillUpdatedUser model route =
    case route of
        UserRoute userRoute ->
            case model.users of
                RemoteData.Success users ->
                    users
                        |> List.filter (\user -> String.toLower userRoute == String.toLower user.email)
                        |> List.head
                        |> Maybe.withDefault emptyUser

                _ ->
                    emptyUser

        _ ->
            emptyUser
