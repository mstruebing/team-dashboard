module Urls
    exposing
        ( customersUrl
        , loginUrl
        , logoutUrl
        , positionsUrl
        , projectsUrl
        , skillsUrl
        , slogansUrl
        , teamsUrl
        , usersUrl
        )


baseUrl : String
baseUrl =
    "http://127.0.0.1:8010/sitegeist.teamdashboard/"


loginUrl : String
loginUrl =
    baseUrl ++ "Authentication/authenticate"


logoutUrl : String
logoutUrl =
    baseUrl ++ "Authentication/logout"


skillsUrl : String
skillsUrl =
    baseUrl ++ "skill"


positionsUrl : String
positionsUrl =
    baseUrl ++ "position"


customersUrl : String
customersUrl =
    baseUrl ++ "customer"


teamsUrl : String
teamsUrl =
    baseUrl ++ "team"


projectsUrl : String
projectsUrl =
    baseUrl ++ "project"


usersUrl : String
usersUrl =
    baseUrl ++ "user"


slogansUrl : String
slogansUrl =
    baseUrl ++ "slogan"
