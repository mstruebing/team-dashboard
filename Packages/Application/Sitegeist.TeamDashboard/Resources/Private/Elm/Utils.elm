module Utils
    exposing
        ( executeOnResponse
        , executeOnResponseAsList
        , getCustomerCount
        , getProjectCount
        , getTeamCount
        , getUserCount
        , getUserNameByEmail
        , mailLink
        , normalizeSKill
        , normalizeShorthand
        , userLink
        )

import Char exposing (..)
import Html exposing (Html, a, text)
import Html.Attributes exposing (href)
import Material.Spinner as Loading
import Models exposing (Customer, Model, Project, Team, User)
import Msgs exposing (Msg)
import RemoteData exposing (RemoteData, WebData)


executeOnResponse : WebData (List a) -> (List a -> Html Msg) -> Html Msg
executeOnResponse response func =
    case response of
        RemoteData.NotAsked ->
            text ""

        RemoteData.Loading ->
            Loading.spinner
                [ Loading.active True
                , Loading.singleColor True
                ]

        RemoteData.Success data ->
            func data

        RemoteData.Failure error ->
            text (toString error)


executeOnResponseAsList : WebData (List a) -> (List a -> List (Html Msg)) -> List (Html Msg)
executeOnResponseAsList response func =
    case response of
        RemoteData.NotAsked ->
            [ text "" ]

        RemoteData.Loading ->
            [ Loading.spinner
                [ Loading.active False
                , Loading.singleColor True
                ]
            ]

        RemoteData.Success data ->
            func data

        RemoteData.Failure error ->
            [ text (toString error) ]


getCount : WebData (List a) -> (a -> Bool) -> String -> Html Msg
getCount response filterFunc label =
    executeOnResponse response
        (\elemList ->
            elemList
                |> List.filter filterFunc
                |> List.length
                |> toString
                |> (++) (label ++ ": ")
                |> text
        )


getUserCount : WebData (List User) -> (User -> Bool) -> Html Msg
getUserCount userResponse filterFunc =
    getCount userResponse filterFunc "Mitarbeiter"


getProjectCount : WebData (List Project) -> (Project -> Bool) -> Html Msg
getProjectCount projectRespone filterFunc =
    getCount projectRespone filterFunc "Projekte"


getTeamCount : WebData (List Team) -> (Team -> Bool) -> Html Msg
getTeamCount teamResponse filterFunc =
    getCount teamResponse filterFunc "Teams"


getCustomerCount : WebData (List Customer) -> (Customer -> Bool) -> Html Msg
getCustomerCount customerResponse filterFunc =
    getCount customerResponse filterFunc "Kunden"


mailLink : String -> Html Msg
mailLink address =
    a [ href <| "mailto:" ++ address ] [ text address ]


userLink : User -> Html Msg
userLink user =
    a [ href <| "#users/" ++ user.email ] [ text user.name ]


normalizeSKill : String -> String
normalizeSKill skill =
    skill
        |> String.filter isAlphaNum
        |> String.toLower


normalizeShorthand : String -> String
normalizeShorthand shorthand =
    shorthand
        |> String.filter (\c -> isUpper c || isLower c || toUpper c == 'Ä' || toUpper c == 'Ö' || toUpper c == 'Ü')
        |> String.toUpper
        |> String.slice 0 3


isAlphaNum : Char -> Bool
isAlphaNum c =
    isDigit c || isUpper c || isLower c


getUserNameByEmail : Model -> String -> String
getUserNameByEmail model email =
    let
        maybeUser =
            getUserByEmail model email
    in
    case maybeUser of
        Just user ->
            user.name

        Nothing ->
            ""


getUserByEmail : Model -> String -> Maybe User
getUserByEmail model email =
    case model.users of
        RemoteData.Success users ->
            users
                |> List.filter (\user -> user.email == email)
                |> List.head

        _ ->
            Nothing
