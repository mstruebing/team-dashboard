module View exposing (view)

import Components.Counter exposing (counter)
import Components.List.Customer exposing (customerList)
import Components.List.Position exposing (positionList)
import Components.List.Project exposing (projectList)
import Components.List.Skill exposing (skillList)
import Components.List.Slogan exposing (sloganList)
import Components.List.Team exposing (teamList)
import Components.List.User exposing (userList)
import Components.Login exposing (login)
import Components.Messages exposing (messages)
import Components.Navigation exposing (navigation)
import Components.SearchResults exposing (searchResults)
import Components.Searchbar exposing (searchbar)
import Components.Single.Customer exposing (singleCustomer)
import Components.Single.Position exposing (singlePosition)
import Components.Single.Project exposing (singleProject)
import Components.Single.Skill exposing (singleSkill)
import Components.Single.Slogan exposing (singleSlogan)
import Components.Single.Team exposing (singleTeam)
import Components.Single.User exposing (singleUser)
import Html exposing (Html, button, div, nav, text)
import Html.Attributes exposing (class)
import List.Extra exposing (getAt)
import Material.Color as Color
import Material.Scheme as Scheme
import Models exposing (Model, Route)
import Msgs exposing (Msg)
import RemoteData


view : Model -> Html Msgs.Msg
view model =
    case model.route of
        Models.IndexRoute ->
            page model Nothing

        Models.CustomerListRoute ->
            customerList model
                |> Just
                |> page model

        Models.CustomerRoute name ->
            singleCustomer model name
                |> page model

        Models.ProjectListRoute ->
            projectList model
                |> Just
                |> page model

        Models.ProjectRoute quoJobNr ->
            singleProject model quoJobNr
                |> page model

        Models.PositionListRoute ->
            positionList model
                |> Just
                |> page model

        Models.PositionRoute label ->
            singlePosition model label
                |> page model

        Models.SkillListRoute ->
            skillList model
                |> Just
                |> page model

        Models.SkillRoute label ->
            singleSkill model label
                |> page model

        Models.SloganListRoute ->
            sloganList model
                |> Just
                |> page model

        Models.TeamListRoute ->
            teamList model
                |> Just
                |> page model

        Models.TeamRoute name ->
            singleTeam model name
                |> page model

        Models.UserListRoute ->
            userList model
                |> Just
                |> page model

        Models.UserRoute userMail ->
            singleUser model userMail
                |> page model

        Models.SloganRoute slogan ->
            singleSlogan model slogan
                |> page model

        Models.NotFoundRoute ->
            text "NOT FOUND"
                |> Just
                |> page model


page : Model -> Maybe (Html Msg) -> Html Msgs.Msg
page model content =
    div []
        [ Scheme.topWithScheme Color.Orange Color.Indigo <|
            div [ class "header" ] <|
                header model
        , div [ class "content" ]
            (if String.isEmpty model.searchQuery then
                case content of
                    Just value ->
                        [ value ]

                    Nothing ->
                        case model.slogans of
                            RemoteData.Success slogans ->
                                let
                                    maybeSlogan =
                                        getAt model.sloganIndex slogans
                                in
                                case maybeSlogan of
                                    Just slogan ->
                                        [ div []
                                            [ counter model
                                            , div [ class "item__wrapper frontSlogan" ]
                                                [ text <| "\"" ++ slogan ++ "\"" ]
                                            ]
                                        ]

                                    Nothing ->
                                        [ counter model ]

                            _ ->
                                [ text "" ]
             else
                [ searchResults model ]
            )
        ]


header : Model -> List (Html Msg)
header model =
    [ login model, searchbar model, messages model, navigation model ]
