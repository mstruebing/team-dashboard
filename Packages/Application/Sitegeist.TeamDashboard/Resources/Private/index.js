// pull in desired CSS/SASS files
require( './Styles/main.scss' );

// inject bundled Elm app into div#main
var Elm = require( './Elm/Main' );

var app = Elm.Main.embed(document.getElementById('main'));
